<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>InsuCal</title>
      <link rel="stylesheet" href="assets/css/login.css">
</head>

<body>
  <div class="login-page">
  <div class="form">
    <img src="assets/img/logo.png" style="width: auto; height: 60px;" alt="">

    <form action="process/check_login.php" method="post" class="login-form">
      <input type="text" name="username" placeholder="ชื่อผู้ใช้งาน"/>
      <input type="password" name="password" placeholder="รหัสผ่าน"/>
      <button type="submit" style="margin-bottom: 15px;">เข้าสู่ระบบ</button>
      <a href="register.php" style="text-decoration: none; font-size: 12px;">[ สมัครสมาชิก ]</a>
      <a href="password.php" style="text-decoration: none; font-size: 12px;">[ ลืมรหัสผ่าน ]</a>
      <p class="message">ระบบบริหารจัดการ InsuCal</p>
    </form>
  </div>
</div>


</body>
</html>
