<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>InsuCal</title>
      <link rel="stylesheet" href="assets/css/login.css">
</head>

<body>
  <div class="login-page" style="padding: 3% 0 0">
  <div class="form">
    <img src="assets/img/logo.png" style="width: auto; height: 60px;" alt="">

    <p class="text-center">สำหรับผู้ป่วยเท่านั้น</p>

    <form action="process/user_process.php" method="post" class="login-form">

      <p>
        <b>เพศ : </b>
        <input type="radio" style="width: 20px;" name="sex" value="M" checked> ชาย &nbsp&nbsp&nbsp
        <input type="radio" style="width: 20px;" name="sex" value="F"> หญิง
      </p>
        <p>
        <b>แนะนำการฉีดอินซูลินโดย : </b><br>
        <input type="radio" style="width: 20px;" name="insulinRec" value="1" checked> โปรแกรม &nbsp&nbsp&nbsp
        <input type="radio" style="width: 20px;" name="insulinRec" value="2"> แพทย์สั่ง
      </p>


      <input type="text" name="name" placeholder="ชื่อ-นามสกุล" required/>
      <input type="text" name="username" placeholder="ชื่อผู้ใช้งาน" required/>
      <input type="password" name="password" placeholder="รหัสผ่าน" required/>
      <input type="password" name="repassword" placeholder="ยืนยันรหัสผ่าน" required/>
      <input type="email" name="email" placeholder="อีเมล์" required/>
      <input type="text" name="tel" placeholder="เบอร์โทรศัพท์" required/>


      <button type="submit" style="margin-bottom: 15px;">สมัครสมาชิก</button>
      <a href="login.php" style="text-decoration: none; font-size: 12px;">[ เข้าสู่ระบบ ]</a>
      <p class="message">ระบบบริหารจัดการ InsuCal</p>
    </form>
  </div>
</div>

</body>
</html>
