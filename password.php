<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>แจ้งลืมรหัสผ่าน</title>
      <link rel="stylesheet" href="assets/css/login.css">
</head>

<body>
  <div class="login-page">
  <div class="form">
    <img src="assets/img/logo.png" style="width: auto; height: 60px;" alt="">

    <form action="process/password.php" method="post" class="login-form">
      <input type="text" name="tel" placeholder="เบอร์โทรศัพท์"/>
      <button type="submit" onclick="return confirm('ยืนยันการส่งคำขอ?')" style="margin-bottom: 15px;">แจ้งลืมรหัสผ่าน</button>
      <a href="login.php" style="text-decoration: none; font-size: 12px;">[ เข้าสู่ระบบ ]</a>
      <p class="message">ระบบบริหารจัดการ InsuCal</p>
    </form>
  </div>
</div>

</body>
</html>
