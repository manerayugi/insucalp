<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">จัดการแพทย์</h4>
            </div>
        </div>

        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">
                    รายการแพทย์ทั้งหมด
                    <a href="doctor_form.php" class="btn btn-primary btn-sm pull-right">เพิ่มข้อมูล</a>
                </div>
                <div class="panel-body">
                    <?PHP
                    $sql = "SELECT * FROM member WHERE member_status = 'DOCTOR'";
                    $query = result_array($sql);
                    ?>
                    <table class="table table-bordered table-striped" id="table-js">
                        <thead>
                        <tr>
                            <th width="50" class="text-center">ลำดับ</th>
                            <th  class="text-center">ชื่อ-นามสกุล</th>
                            <th width="90" class="text-center">เบอร์โทร</th>
                            <th width="50" class="text-center">เพศ</th>
                            <th class="text-center">อีเมล์</th>
                            <th width="100" class="text-center">สถานะ</th>
                            <th width="100" class="text-center">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?PHP foreach ($query as $key=>$row) { ?>
                            <tr>
                                <td class="text-center"><?=$key+1;?></td>
                                <td class="text-center"><?=$row['member_name'];?></td>
                                <td class="text-center"><?=$row['member_tel'];?></td>
                                <td class="text-center"><?=$row['member_sex'];?></td>
                                <td class="text-center"><?=$row['member_email'];?></td>
                                <td class="text-center"><?=member_status($row['member_status']);?></td>
                                <td class="text-center">
                                    <a href="doctor_form.php?id=<?=$row['member_id'];?>" class="btn btn-warning btn-xs">แก้ไข</a>
                                    <a href="process/delete.php?table=member&ff=member_id&id=<?=$row['member_id'];?>" onclick="return confirm('ยืนยันการลบ')" class="btn btn-danger btn-xs">ลบ</a>
                                </td>
                            </tr>
                        <?PHP } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
