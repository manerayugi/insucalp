<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
      integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<head>
    <?PHP include 'include/script.php'; ?>
    <link rel="stylesheet" href="assets/js/time/jquery.ui.timepicker.css"/>
    <link rel="stylesheet" href="assets/js/time/ui-lightness/jquery-ui-1.10.0.custom.min.css"/>
    <script src="assets/js/time/jquery.ui.timepicker.js"></script>
    <style>
        div.ui-datepicker {
            font-size: 14px;
        }

        .ui-datepicker-month, .ui-datepicker-year {
            color: red;
        }
    </style>

    <script>
        $(document).ready(function () {

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });


            $('.timepicker').timepicker({
                onHourShow: OnHourShowCallback
            });

            function OnHourShowCallback(hour) {
                return true; // valid
            }


        });


    </script>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">ตารางรายงานผล</h4>
            </div>
        </div>

        <?PHP
        $member_id = check_session('member_id');

        if (check_session("member_status") != "USER") {
            if (isset($_GET['member_id'])) {
                $member_id = $_GET['member_id'];
            }
        }

        ?>


        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ประวัติการบันทึกผลทั้งหมด
                </div>
                <div class="panel-body">

                    <?PHP if (check_session("member_status") != "USER") { ?>
                        <form action="" method="get">


                            <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                                <label class="col-md-3 control-label"
                                       style="text-align: right; padding-top: 5px;">สมาชิก *</label>

                                <div class="col-md-5" style="padding-top: 4px;">
                                    <?PHP
                                    $sql = "SELECT * FROM member WHERE member_status != 'DOCTOR'";
                                    $member = result_array($sql);
                                    ?>
                                    <select name="member_id" class="form-control" required>
                                        <option disabled selected value="">เลือกสมาชิก</option>
                                        <?PHP foreach ($member as $mb) { ?>
                                            <option <?= $member_id == $mb['member_id'] ? "selected" : ""; ?>
                                                    value="<?= $mb['member_id'] ?>"><?= $mb['member_name'] ?></option>
                                        <?PHP } ?>
                                    </select>
                                </div>

                                <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>
                    <?PHP } ?>


                    <?PHP
                    $start_date = date("Y-m-d", strtotime("-3 day"));
                    $start_time = "00:00:00";

                    $end_date = date("Y-m-d");
                    $end_time = date("H:i:s");

                    if (isset($_GET['start_date'])) {
                        $start_date = $_GET['start_date'];
                    }

                    if (isset($_GET['start_time'])) {
                        $start_time = $_GET['start_time'];
                    }

                    if (isset($_GET['end_date'])) {
                        $end_date = $_GET['end_date'];
                    }

                    if (isset($_GET['end_time'])) {
                        $end_time = $_GET['end_time'];
                    }

                    $start = "{$start_date} {$start_time}";
                    $end = "{$end_date} {$end_time}";


                    ?>
                    <form action="" method="get">

                        <input type="hidden" name="member_id" value="<?= $member_id; ?>">

                        <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                            <label class="col-md-2 control-label"
                                   style="text-align: right; padding-top: 5px;">ช่วงเวลา *</label>

                            <div class="col-md-2" style="padding-top: 4px;">
                                <input type="text" name="start_date" value="<?= $start_date; ?>"
                                       class="form-control datepicker" placeholder="วันที่เริ่มต้น" required>
                            </div>
                            <div class="col-md-2" style="padding-top: 4px;">
                                <input type="text" name="start_time" value="<?= $start_time; ?>"
                                       class="form-control timepicker" placeholder="เวลาเริ่มต้น" required>
                            </div>
                            <label class="col-md-1 control-label"
                                   style="text-align: center; padding-top: 5px;">ถึง</label>

                            <div class="col-md-2" style="padding-top: 4px;">
                                <input type="text" name="end_date" value="<?= $end_date; ?>"
                                       class="form-control datepicker" placeholder="วันที่สิ้นสุด" required>
                            </div>
                            <div class="col-md-2" style="padding-top: 4px;">
                                <input type="text" name="end_time" value="<?= $end_time; ?>"
                                       class="form-control timepicker" placeholder="เวลาสิ้นสุด" required>
                            </div>

                            <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>


                        <?PHP
                        $sql = "SELECT * FROM (
    (SELECT DATE(date_time) as dates , date_time as datetimes , duration_id , bloodsugar_number , bloodsugar_result as insulin_number , 0 as title , 1 as statuss , rISF as realInsulin  FROM bloodsugar WHERE member_id = '{$member_id}'AND date_time BETWEEN '{$start}' AND '{$end}')
    UNION ALL
    (SELECT DATE(eat_datetime) as dates , eat_datetime as datetimes , duration_id , '-' as bloodsugar_number , insulin_number , eat_id as title , insulin_status as statuss , rinsulin as realInsulin  FROM eat WHERE member_id = '{$member_id}'AND eat_datetime BETWEEN '{$start}' AND '{$end}')
) results
ORDER BY  datetimes ASC";

                        $query = result_array($sql);

                        ?>
                    </form>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="100" class="text-center">วันที่</th>
                            <th width="120" class="text-center">ช่วง</th>
                            <th width="120" class="text-center">น้ำตาล ก่อน</th>
                            <th width="120" class="text-center">น้ำตาล หลัง</th>
                            <th width="120" class="text-center">อินซูลีนที่แนะนำ</th>
                            <th width="120" class="text-center">อินซูลีนที่ฉีดจริง</th>
                            <th class="text-center">รายการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?PHP foreach ($query as $key => $row) {
                            ?>
                            <tr>
                                <td class="text-center"><?= $row['dates']; ?></td>
                                <td class="text-center"><?= duration_name($row['duration_id']); ?></td>
                                <td class="text-center"><? if ($row['duration_id'] == 2 OR
                                        $row['duration_id'] == 4 OR
                                        $row['duration_id'] == 6) {
                                        echo "-";
                                    } else {
                                        echo $row['bloodsugar_number'] . " ";
                                        if ($row['bloodsugar_number'] > 0) {
                                            echo duraicon($row['duration_id']);
                                        }
                                    } ?> </td>
                                <td class="text-center"><? if ($row['duration_id'] == 2 OR
                                        $row['duration_id'] == 4 OR
                                        $row['duration_id'] == 6) {
                                        echo $row['bloodsugar_number'] . " ";
                                        if ($row['bloodsugar_number'] > 0) {
                                            echo duraicon($row['duration_id']);
                                        }
                                    } else {
                                        echo "-";
                                    } ?></td>
                                <td class="text-center">
                                    <?= $row['insulin_number']; ?>
                                    <? //= statuss($row['statuss']); ?>
                                </td>
                                <td class="text-center">
                                    <?= $row['realInsulin']; ?>
                                    <? //= statuss($row['statuss']); ?>
                                </td>
                                <td class="text-left">
                                    <?PHP if ($row['title'] == 0) { ?>
                                        <b> บันทึกระดับน้ำตาลในเลือด</b>

                                        <?PHP if ($row['duration_id'] > 6) { ?>
                                            <ul style="font-size: 12px; list-style: none; padding: 0;">
                                                <li>- เวลาบันทึก : <?= $row['datetimes']; ?></li>
                                            </ul>
                                        <?PHP } ?>

                                    <?PHP } else { ?>
                                        <?PHP
                                        $sql = "SELECT * FROM eat WHERE eat_id = '{$row['title']}'";
                                        $eat = row_array($sql);
                                        ?>

                                        <b> บันทึกการทานอาหาร <?= $eat['eat_carb']; ?> g ดังนี้ </b>
                                        <?PHP
                                        $sql = "SELECT * FROM eat_detail a INNER JOIN food b ON a.food_id = b.food_id WHERE eat_id = '{$row['title']}'";
                                        $cc = result_array($sql);
                                        ?>

                                        <ul style="font-size: 12px; list-style: none; padding: 0;">
                                            <?PHP foreach ($cc as $_cc) { ?>
                                                <li>- <?= $_cc['food_name']; ?> : <?= $_cc['eat_detail_carb']; ?> g, <?= $_cc['eat_detail_qty']; ?> หน่วย รวม <?= $_cc['eat_detail_qty']*$_cc['eat_detail_carb']; ?> g</li>
                                            <?PHP } ?>
                                        </ul>
                                    <?PHP } ?>

                                </td>
                            </tr>
                        <?PHP } ?>
                        </tbody>
                    </table>
                    <form action="" method="get">
                        <input type="hidden" name="bl_max" value="<?= $bl_max1; ?>">
                    </form>
                    <form role="form" action="print_report.php" class="" method="post"
                          enctype="multipart/form-data">
                        <input type="hidden" name="blood_max" value="<?= $bl_max1; ?>">
                    </form>
                    <center>
                        <a target="_blank"
                           href="print_report.php?member_id=<?= $member_id; ?>&start=<?= $start; ?>&end=<?= $end; ?>"
                           class="btn btn-lg btn-info btn-rounded">
                            พิมพ์รายงาน
                        </a>
                    </center>

                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
