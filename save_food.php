<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">บันทึกการทานอาหาร</h4>
            </div>
        </div>


        <?PHP
        $member_id = check_session('member_id');
        $mid = check_session("member_id");

        if (check_session("member_status") == "ADMIN") {
            if (isset($_GET['member_id'])) {
                $member_id = $_GET['member_id'];
            }
        }

        ?>

        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ประวัติบันทึกการทานอาหารทั้งหมด
                    <?PHP if ($mid == $member_id) { ?>
                        <a href="save_food_form.php?member_id=<?= $member_id; ?>&clear"
                           class="btn btn-primary btn-sm pull-right">เพิ่มข้อมูลการรับประทานอาหาร</a>
                    <?PHP } ?>
                </div>
                <div class="panel-body">

                    <?PHP if (check_session("member_status") == "ADMIN") { ?>
                        <form action="" method="get">


                            <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                                <label class="col-md-3 control-label"
                                       style="text-align: right; padding-top: 5px;">สมาชิก *</label>

                                <div class="col-md-5" style="padding-top: 4px;">
                                    <?PHP
                                    $sql = "SELECT * FROM member WHERE member_status != 'DOCTOR'";
                                    $member = result_array($sql);
                                    ?>
                                    <select name="member_id" class="form-control" required>
                                        <option disabled selected value="">เลือกสมาชิก</option>
                                        <?PHP foreach ($member as $mb) { ?>
                                            <option <?= $member_id == $mb['member_id'] ? "selected" : ""; ?>
                                                value="<?= $mb['member_id'] ?>"><?= $mb['member_name'] ?></option>
                                        <?PHP } ?>
                                    </select>
                                </div>

                                <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>
                    <?PHP } ?>


                    <?PHP
                    $sql = "SELECT * FROM eat a INNER JOIN duration b ON a.duration_id = b.duration_id INNER JOIN member d ON a.member_id = d.member_id WHERE a.member_id = '{$member_id}' order by eat_datetime desc";
                    $query = result_array($sql);
                    ?>
                    <table class="table table-bordered table-striped" id="table-js">
                        <thead>
                        <tr>
                            <th width="50" class="text-center">ลำดับ</th>
                            <th class="text-center">ชื่อ-นามสกุล</th>
                            <th width="120" class="text-center">ช่วงเวลา</th>
                            <th width="200" class="text-center">คาร์โบไฮเดรตทั้งหมด</th>
                            <th width="120" class="text-center">อินซูลีนที่แนะนำ</th>
                            <th width="120" class="text-center">อินซูลีนที่ฉีดจริง</th>
                            <th width="90" class="text-center">วันที่บันทึก</th>
                            <th width="90" class="text-center">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?PHP foreach ($query as $key => $row) { ?>
                            <tr>
                                <td class="text-center"><?= $key + 1; ?></td>
                                <td class="text-center"><?= $row['member_name']; ?></td>
                                <td class="text-center"><?= $row['duration_name']; ?></td>
                                <td class="text-left">
                                    <b><?= $row['eat_carb']; ?> g ดังนี้</b>
                                    <?PHP
                                    $sql = "SELECT * FROM eat_detail a INNER JOIN food b ON a.food_id = b.food_id WHERE eat_id = '{$row['eat_id']}'";
                                    $cc = result_array($sql);
                                    ?>

                                    <ul style="text-align: left; font-size: 12px; list-style: none; padding: 0;">
                                        <?PHP foreach ($cc as $_cc) { ?>
                                            <li>- <?= $_cc['food_name']; ?> : <?= $_cc['eat_detail_carb']; ?> g, <?= $_cc['eat_detail_qty']; ?> หน่วย รวม <?= $_cc['eat_detail_qty']*$_cc['eat_detail_carb']; ?> g</li>
                                        <?PHP } ?>
                                    </ul>
                                </td>
                                <td class="text-center"><?= $row['insulin_number']; ?> Unit</td>
                                <td class="text-center"><?= $row['rinsulin']; ?> Unit</td>
                                <td class="text-center"><?= $row['eat_datetime']; ?></td>
                                <td class="text-center">
                                    <?PHP if ($mid == $member_id) { ?>
                                        <a href="process/delete.php?table=eat&ff=eat_id&id=<?= $row['eat_id']; ?>"
                                           onclick="return confirm('ยืนยันการลบ')" class="btn btn-danger btn-xs">ลบ</a>
                                    <?PHP } else {
                                        echo '-';
                                    } ?>
                                </td>
                            </tr>
                        <?PHP } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
