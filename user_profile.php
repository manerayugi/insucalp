<?php
/**
 * Created by PhpStorm.
 * User: Phantom OF Time
 * Date: 7/15/2018
 * Time: 10:06 PM
 */
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
    <?PHP date_default_timezone_set('Asia/Bangkok'); ?>


</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">ข้อมูลส่วนตัว</h4>
            </div>
        </div>

        <?PHP
        $id = check_session("member_id");
        $url = "index.php";

        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            $url = "user.php";
        }

        $sql = "SELECT * FROM member WHERE member_id = '{$id}'";
        $member = row_array($sql);

        ?>

        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="form-group">
                            <label class="control-label col-md-3">ข้อมูลส่วนตัว : </label>
                            <div class="col-md-3"><?= $member['member_name']; ?></div>
                            <label class="control-label col-md-3">ชื่อผู้ใช้ : </label>
                            <div class="col-md-3"><?= $member['member_username']; ?></div>
                            <!--                        ข้อมูลส่วนตัว : -->
                            <? //= $member['member_name']; ?><!--  <br>ชื่อผู้ใช้งาน : --><? //= $member['member_username']; ?>
                        </div>
                    </div>

                    <?PHP

                    $check = 1;

                    $weight = '';
                    $height = '';
                    $ICR_br = '';
                    $ICR_lu = '';
                    $ICR_di = '';
                    $ISF_br = '';
                    $ISF_lu = '';
                    $ISF_di = '';
                    $MAX_blsu = '';
                    $doctor = '';
                    $age = '';
                    $bday = '';
                    $bmonth = '';
                    $byear = '';


                    $sql = "SELECT * FROM information WHERE member_id = '{$id}'";
                    $query = row_array($sql);

                    if ($query) {

                        $check = 0;

                        $age = $query['age'];
                        $weight = $query['weight'];
                        $height = $query['height'];
                        $ICR_br = $query['ICR_br'];
                        $ICR_lu = $query['ICR_lu'];
                        $ICR_di = $query['ICR_di'];
                        $ISF_br = $query['ISF_br'];
                        $ISF_lu = $query['ISF_lu'];
                        $ISF_di = $query['ISF_di'];
                        $MAX_blsu = $query['MAX_blsu'];
                        $doctor = $query['doctor'];
                        $bday = $query['bday'];
                        $bmonth = $query['bmount'];
                        $byear = $query['byear'];
                        $itype = $query['itype'];
                    }

                    $s = "select * from member WHERE member_id = '{$id}'";
                    $q = row_array($s);

                    $name = $q['member_name'];
                    $username = $q['member_username'];
                    $password = $q['member_password'];
                    $tel = $q['member_tel'];
                    $email = $q['member_email'];
                    $sex = $q['member_sex'];


                    ?>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="col-md-12">
                                    <form role="form" action="process/user_update.php" method="post"
                                          enctype="multipart/form-data">

                                        <input type="hidden" name="id" value="<?= $id ?>">
                                        <input type="hidden" name="check" value="<?= $check ?>">
                                        <input type="hidden" name="url" value="<?= $url ?>">

                                        <?PHP if ($check) { ?>
                                            <p class="text-center" style="color: red;">
                                                *** <?= $member['member_name']; ?> :
                                                ยังไม่เคยกรอกข้อมูล ***</p>
                                        <?PHP } ?>

                                        <div class="form-group">
                                            <label>แพทย์ประจำตัว : </label>
                                            <?PHP
                                            if ($query['doctor'] == 0) {
                                                echo "ไม่มีแพทย์ประจำตัว";
                                            } else {
                                                //echo $query['doctor'];
                                                $sql = "SELECT * FROM member WHERE member_id = '{$query['doctor']}'";
                                                $member1 = row_array($sql);
                                                echo $member1['member_name'];
                                            }
                                            ?>
                                        </div>

                                        <div class="form-group">
                                            <label>วัน/เดือน/ปี เกิด : </label>
                                            <span> <?PHP echo $query['bday'] . ' ' . mount_name($query['bmount']) . ' ' . $query['byear']; ?> </span>

                                        </div>
                                        <div class="form-group">
                                            <label>เพศ : </label>
                                            <span> <?PHP if ($q['member_sex'] == 'M') {
                                                    echo "ชาย";
                                                } else {
                                                    echo "หญิง";
                                                }; ?> </span>

                                        </div>


                                        <div class="form-group">
                                            <label>อายุ : </label>
                                            <span> <?PHP echo getAgeYMD($query['bday'], $query['bmount'], $query['byear']); ?> </span>
                                        </div>

                                        <div class="form-group">
                                            <label>น้ำหนัก : </label>
                                            <span><?PHP echo $weight . " กิโลกรัม"; ?></span>
                                        </div>

                                        <div class="form-group">
                                            <label>ส่วนสูง : </label>
                                            <span> <?PHP echo $height . " เซนติเมตร"; ?></span>
                                        </div>

                                        <div class="form-group">
                                            <label>ประเภทของอินซูลินที่ใช้ : </label>
                                            <span> <?PHP if ($itype == 1) {
                                                    echo "อินซูลินออกฤทธิ์เร็ว(Rapid-acting insulin analog)";
                                                } else {
                                                    echo "อินซูลินออกฤทธิ์สั้น(Short-acting insulin)";
                                                } ?></span>
                                        </div>

                                        <div class="form-group">
                                            <label>อินซูลินที่ควรได้รับต่อวัน : </label>
                                            <span> <?PHP echo calTDD($age, $weight) . " unit"; ?></span>
                                        </div>

                                        <div class="form-group">
                                            <label>อินซูลินต่อกรมของคาร์โบไฮเดตร(ICR) <i style="color: red;">( 1 unit
                                                    ต่อคาร์โบไฮเดรต xxx
                                                    กรัม ) </i></label><br>
                                            <center><span><strong>เช้า</strong> 1:<?PHP echo $ICR_br; ?>
                                                    <strong>กลางวัน</strong> 1:<?PHP echo $ICR_lu; ?>
                                                    <strong>เย็น</strong> 1:<?PHP echo $ICR_di; ?></span></center>
                                        </div>


                                        <div class="form-group">
                                            <label>อินซูลินเพื่อแก้ไขน้ำตาลในเลือดสูง(ISF) <i style="color: red;">( 1
                                                    unit ต่อระดับน้ำตาลในเลือดที่มากกว่าระดับน้ำตาลเป้าหมายทุก ๆ xxx
                                                    mg/dL ) </i></label><br>
                                            <center><span><strong>เช้า</strong> 1:<?PHP echo $ISF_br; ?>
                                                    <strong>กลางวัน</strong> 1:<?PHP echo $ISF_lu; ?>
                                                    <strong>เย็น</strong> 1:<?PHP echo $ISF_di; ?></span></center>
                                        </div>


                                        <div class="form-group">
                                            <label>ระดับน้ำตาลในเลือดเป้าหมาย : </label>
                                            <span> <?PHP echo $MAX_blsu . " mg/dL"; ?></span>
                                        </div>

                                        <div class="form-group">
                                            <label>เบอร์โทรศัพท์ : </label>
                                            <span> <?PHP echo $tel; ?></span>
                                        </div>

                                        <div class="form-group">
                                            <label>E-mail : </label>
                                            <span> <?PHP echo $email; ?></span>
                                        </div>

                                        <div align="center">
                                            <a href="edit_profile.php" style="margin-right: 10px;"
                                               class="btn btn-warning pull-right"><i
                                                        class="fa fa-wrench"></i> แก้ไขข้อมูลส่วนตัว</a>
                                            <a href="update_user.php" style="margin-right: 10px;"
                                               class="btn btn-success pull-right"><i
                                                        class="fa fa-check"></i> อัพเดทการรักษา</a>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?PHP include 'include/footer.php'; ?>


</body>
</html>

