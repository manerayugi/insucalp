<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
    <?PHP date_default_timezone_set("Asia/Bangkok"); ?>

    <link rel="stylesheet" href="assets/js/time/jquery.ui.timepicker.css"/>
    <link rel="stylesheet" href="assets/js/time/ui-lightness/jquery-ui-1.10.0.custom.min.css"/>
    <script src="assets/js/time/jquery.ui.timepicker.js"></script>

    <style>
        div.ui-datepicker {
            font-size: 14px;
        }

        .ui-datepicker-month, .ui-datepicker-year {
            color: red;
        }
    </style>

    <script>
        $(document).ready(function () {

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });


            $('.timepicker').timepicker({
                onHourShow: OnHourShowCallback
            });

            function OnHourShowCallback(hour) {
                return true; // valid
            }


        });


    </script>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">

        <?PHP
        $member_id = check_session('member_id');

        if (check_session("member_status") != "USER") {
            if (isset($_GET['member_id'])) {
                $member_id = $_GET['member_id'];
            }
        }


        $start_date = date("Y-m-d", strtotime("-3 day"));
        $start_time = "00:00:00";

        $end_date = date("Y-m-d");
        $end_time = date("H:i:s");

        if (isset($_GET['start_date'])) {
            $start_date = $_GET['start_date'];
        }

        if (isset($_GET['start_time'])) {
            $start_time = $_GET['start_time'];
        }

        if (isset($_GET['end_date'])) {
            $end_date = $_GET['end_date'];
        }

        if (isset($_GET['end_time'])) {
            $end_time = $_GET['end_time'];
        }

        $start = "{$start_date} {$start_time}";
        $end = "{$end_date} {$end_time}";

        ?>


        <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        กราฟแสดงระดับน้ำตาลในเลือด
                    </div>
                    <div class="panel-body" style="min-height: 330px; overflow: hidden;">
                        <?PHP if (check_session("member_status") != "USER") { ?>
                            <form action="" method="get">


                                <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                                    <label class="col-md-3 control-label"
                                           style="text-align: right; padding-top: 5px;">สมาชิก *</label>

                                    <div class="col-md-5" style="padding-top: 4px;">
                                        <?PHP
                                        $sql = "SELECT * FROM member WHERE member_status != 'DOCTOR'";
                                        $member = result_array($sql);
                                        ?>
                                        <select name="member_id" class="form-control" required>
                                            <option disabled selected value="">เลือกสมาชิก</option>
                                            <?PHP foreach ($member as $mb) { ?>
                                                <option <?= $member_id == $mb['member_id'] ? "selected" : ""; ?>
                                                        value="<?= $mb['member_id'] ?>"><?= $mb['member_name'] ?></option>
                                            <?PHP } ?>
                                        </select>
                                    </div>

                                    <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <hr>
                        <?PHP } ?>

                        <form action="" method="get">

                            <input type="hidden" name="member_id" value="<?= $member_id; ?>">

                            <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                                <label class="col-md-2 control-label"
                                       style="text-align: right; padding-top: 5px;">ช่วงเวลา *</label>

                                <div class="col-md-2" style="padding-top: 4px;">
                                    <input type="text" name="start_date" value="<?= $start_date; ?>"
                                           class="form-control datepicker" placeholder="วันที่เริ่มต้น" required>
                                </div>
                                <div class="col-md-2" style="padding-top: 4px;">
                                    <input type="text" name="start_time" value="<?= $start_time; ?>"
                                           class="form-control timepicker" placeholder="เวลาเริ่มต้น" required>
                                </div>
                                <label class="col-md-1 control-label"
                                       style="text-align: center; padding-top: 5px;">ถึง</label>

                                <div class="col-md-2" style="padding-top: 4px;">
                                    <input type="text" name="end_date" value="<?= $end_date; ?>"
                                           class="form-control datepicker" placeholder="วันที่สิ้นสุด" required>
                                </div>
                                <div class="col-md-2" style="padding-top: 4px;">
                                    <input type="text" name="end_time" value="<?= $end_time; ?>"
                                           class="form-control timepicker" placeholder="เวลาสิ้นสุด" required>
                                </div>

                                <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>

                        <div id="chart1" style="width: 100%; height: 400px; margin: 0 auto"
                             align="center"></div>

                        <hr>

                        <div id="chart2" style="width: 100%; height: 400px; margin: 0 auto"
                             align="center"></div>

                        <hr>

                        <div id="chart3" style="width: 100%; height: 400px; margin: 0 auto"
                             align="center"></div>

                        <hr>

                        <div id="chart4" style="width: 100%; height: 400px; margin: 0 auto"
                             align="center"></div>

                    </div>
                </div>
            </div>

        </div>


    </div>
</div>

<?PHP include 'include/footer.php'; ?>





<?PHP


$sql = "SELECT * FROM bloodsugar WHERE member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_bloodsugar = array();

foreach ($result as $key => $row) {
    $json_bloodsugar[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_bloodsugar[$key][1] = $row['bloodsugar_number'];
}


$json_bloodsugar = json_encode($json_bloodsugar, JSON_NUMERIC_CHECK);

?>

<?PHP
$sql = "SELECT * FROM bloodsugar WHERE member_id = '{$member_id}' AND rISF > 0 AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC";
$result = result_array($sql);

$json_bloodsugar_result = array();

foreach ($result as $key => $row) {
    $json_bloodsugar_result[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_bloodsugar_result[$key][1] = $row['rISF'];
}

$json_bloodsugar_result = json_encode($json_bloodsugar_result, JSON_NUMERIC_CHECK);
?>

<?PHP
$sql = "SELECT * FROM eat WHERE member_id = '{$member_id}' AND eat_datetime BETWEEN '{$start}' AND '{$end}' ORDER BY eat_datetime ASC";
$result = result_array($sql);

$json_bloodsugar_carb = array();

foreach ($result as $key => $row) {
    $json_bloodsugar_carb[$key][0] = strtotime("+420 minutes", strtotime($row['eat_datetime'])) * 1000;
    $json_bloodsugar_carb[$key][1] = $row['eat_carb'];
}

$json_bloodsugar_carb = json_encode($json_bloodsugar_carb, JSON_NUMERIC_CHECK);
?>

<?PHP
$sql = "SELECT * FROM eat WHERE member_id = '{$member_id}' AND insulin_status = 1 AND eat_datetime BETWEEN '{$start}' AND '{$end}' ORDER BY eat_datetime ASC";
$result = result_array($sql);

$json_bloodsugar_in = array();

foreach ($result as $key => $row) {
    $json_bloodsugar_in[$key][0] = strtotime("+420 minutes", strtotime($row['eat_datetime'])) * 1000;
    $json_bloodsugar_in[$key][1] = $row['rinsulin'];
}

$json_bloodsugar_in = json_encode($json_bloodsugar_in, JSON_NUMERIC_CHECK);


$sql = "SELECT * FROM information WHERE member_id = '{$member_id}'";
$query = row_array($sql);

if ($query) {

    $check = 0;

    $age = $query['age'];
    $weight = $query['weight'];
    $height = $query['height'];
    $ICR_br = $query['ICR_br'];
    $ICR_lu = $query['ICR_lu'];
    $ICR_di = $query['ICR_di'];
    $ISF_br = $query['ISF_br'];
    $ISF_lu = $query['ISF_lu'];
    $ISF_di = $query['ISF_di'];
    $MAX_blsu = $query['MAX_blsu'];
    $doctor = $query['doctor'];
    $bday = $query['bday'];
    $bmount = $query['bmount'];
    $byear = $query['byear'];
    $itype = $query['itype'];
    $tbrs = $query['tbrs'];
    $tbre = $query['tbre'];
    $tlus = $query['tlus'];
    $tlue = $query['tlue'];
    $tdis = $query['tdis'];
    $tdie = $query['tdie'];

}
?>

<script>

    $(function () {
        $('#chart1').highcharts({
            chart: {},
            title: {
                text: 'กราฟสรุป'
            },
            subtitle: {
                text: 'ผลการสรุปดังนี้',
            },
            xAxis: {
                type: 'datetime',
            },
            yAxis: {
                title: {
                    text: null
                },
                plotBands: [
                    {
                        color: '#ffe6e6',
                        from: 0,
                        to: 70
                    },
                    {
                        color: '#FCFFC5',
                        from: 70,
                        to: 80
                    },
                    {
                        color: '#e6ffe6',
                        from: 80,
                        to: <?=$MAX_blsu;?>
                    },
                    {
                        color: '#FCFFC5',
                        from: <?=$MAX_blsu;?>,
                        to: 180
                    },
                    {
                        color: '#ffe6e6',
                        from: 180,
                        to: 1000
                    }
                ]
            },
            series: [

                {
                    tooltip: {
                        valueSuffix: ' mg/dL',
                    },
                    name: 'ระดับน้ำตาลในเลือด',
                    data: <?=$json_bloodsugar;?>,
                    marker: {
                        fillColor: 'white',
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[0]
                    }
                },
                {
                    tooltip: {
                        valueSuffix: ' Unit',
                    },
                    name: 'อินซูลีนแก้ไขภาวะน้ำตาลสูง (ISF)',
                    data: <?=$json_bloodsugar_result;?>,
                    visible: false,
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                    }
                },
                {
                    tooltip: {
                        valueSuffix: ' g',
                    },
                    name: 'ปริมานคาร์โบไฮเดรต',
                    data: <?=$json_bloodsugar_carb;?>,
                    visible: false,
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                    }
                },
                {
                    tooltip: {
                        valueSuffix: 'Unit',
                    },
                    name: 'อินซูลีนต่อคาร์โบไฮเดรต (ICR)',
                    data: <?=$json_bloodsugar_in;?>,
                    visible: false,
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                    }
                }
            ]

        });
    });
</script>


<?PHP

$sql = "SELECT * FROM bloodsugar WHERE duration_id = 1 AND member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_b = array();

foreach ($result as $key => $row) {
    $json_b[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_b[$key][1] = $row['bloodsugar_number'];
}


$json_b = json_encode($json_b, JSON_NUMERIC_CHECK);

$sql = "SELECT * FROM bloodsugar WHERE duration_id = 2 AND member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_a = array();

foreach ($result as $key => $row) {
    $json_a[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_a[$key][1] = $row['bloodsugar_number'];
}


$json_a = json_encode($json_a, JSON_NUMERIC_CHECK);

?>

<script>
    $(function () {
        $('#chart2').highcharts({
            chart: {},
            title: {
                text: 'กราฟระดับน้ำตาลในเลือดก่อนหลังอาหารเช้า'
            },
            subtitle: {
                text: 'ผลการสรุปดังนี้',
                x: -20
            },
            xAxis: {
                type: 'datetime',
            },
            yAxis: {
                title: {
                    text: null
                },
                plotBands: [
                    {
                        color: '#FCFFC5',
                        from: 110,
                        to: 180
                    },
                    {
                        color: '#e6ffe6',
                        from: 70,
                        to: 110
                    },
                    {
                        color: '#ffe6e6',
                        from: 0,
                        to: 70
                    },
                    {
                        color: '#ffe6e6',
                        from: 180,
                        to: 1000
                    }
                ]
            },
            tooltip: {
                valueSuffix: ' mg/dL'
            },
            series: [
                {
                    name: 'ก่อนอาหารเช้า',
                    data: <?=$json_b;?>,
                    color: Highcharts.getOptions().colors[5],
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[5]
                    }
                },
                {
                    name: 'หลังอาหารเช้า',
                    data: <?=$json_a;?>,
                    color: Highcharts.getOptions().colors[6],
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[6]
                    }
                }
            ]

        });
    });
</script>


<?PHP

$sql = "SELECT * FROM bloodsugar WHERE duration_id = 3 AND member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_b = array();

foreach ($result as $key => $row) {
    $json_b[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_b[$key][1] = $row['bloodsugar_number'];
}


$json_b = json_encode($json_b, JSON_NUMERIC_CHECK);

$sql = "SELECT * FROM bloodsugar WHERE duration_id = 4 AND member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_a = array();

foreach ($result as $key => $row) {
    $json_a[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_a[$key][1] = $row['bloodsugar_number'];
}


$json_a = json_encode($json_a, JSON_NUMERIC_CHECK);

?>

<script>
    $(function () {
        $('#chart3').highcharts({
            chart: {},
            title: {
                text: 'กราฟระดับน้ำตาลในเลือดก่อนหลังอาหารกลางวัน'
            },
            subtitle: {
                text: 'ผลการสรุปดังนี้',
                x: -20
            },
            xAxis: {
                type: 'datetime',
            },
            yAxis: {
                title: {
                    text: null
                },
                plotBands: [
                    {
                        color: '#FCFFC5',
                        from: 110,
                        to: 180
                    },
                    {
                        color: '#e6ffe6',
                        from: 70,
                        to: 110
                    },
                    {
                        color: '#ffe6e6',
                        from: 0,
                        to: 70
                    },
                    {
                        color: '#ffe6e6',
                        from: 180,
                        to: 1000
                    }
                ]
            },
            tooltip: {
                valueSuffix: ' mg/dL'
            },
            series: [
                {
                    name: 'ก่อนอาหารกลางวัน',
                    data: <?=$json_b;?>,
                    color: Highcharts.getOptions().colors[7],
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[7]
                    }
                },
                {
                    name: 'หลังอาหารกลางวัน',
                    data: <?=$json_a;?>,
                    color: Highcharts.getOptions().colors[8],
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[8]
                    }
                }
            ]

        });
    });
</script>


<?PHP

$sql = "SELECT * FROM bloodsugar WHERE duration_id = 5 AND member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_b = array();

foreach ($result as $key => $row) {
    $json_b[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_b[$key][1] = $row['bloodsugar_number'];
}


$json_b = json_encode($json_b, JSON_NUMERIC_CHECK);

$sql = "SELECT * FROM bloodsugar WHERE duration_id = 6 AND member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY date_time ASC ";
$result = result_array($sql);

$json_a = array();

foreach ($result as $key => $row) {
    $json_a[$key][0] = strtotime("+420 minutes", strtotime($row['date_time'])) * 1000;
    $json_a[$key][1] = $row['bloodsugar_number'];
}


$json_a = json_encode($json_a, JSON_NUMERIC_CHECK);

?>

<script>
    $(function () {
        $('#chart4').highcharts({
            chart: {},
            title: {
                text: 'กราฟระดับน้ำตาลในเลือดก่อนหลังอาหารเย็น'
            },
            subtitle: {
                text: 'ผลการสรุปดังนี้',
                x: -20
            },
            xAxis: {
                type: 'datetime',
            },
            yAxis: {
                title: {
                    text: null
                },
                plotBands: [
                    {
                        color: '#FCFFC5',
                        from: 110,
                        to: 180
                    },
                    {
                        color: '#e6ffe6',
                        from: 70,
                        to: 110
                    },
                    {
                        color: '#ffe6e6',
                        from: 0,
                        to: 70
                    },
                    {
                        color: '#ffe6e6',
                        from: 180,
                        to: 1000
                    }
                ]
            },
            tooltip: {
                valueSuffix: ' mg/dL'
            },
            series: [
                {
                    name: 'ก่อนอาหารเย็น',
                    data: <?=$json_b;?>,
                    color: Highcharts.getOptions().colors[9],
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[9]
                    }
                },
                {
                    name: 'หลังอาหารเย็น',
                    data: <?=$json_a;?>,
                    color: Highcharts.getOptions().colors[10],
                    marker: {
                        enabled: true,
                        lineWidth: 1,
                        lineColor: Highcharts.getOptions().colors[10]
                    }
                }
            ]

        });
    });
</script>

</body>
</html>
