<?php


//session
session_start();

function check_session($key)
{
    return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
}

function check_login($SS, $url)
{
    if (check_session($SS)) {

    } else {
        header('Location:' . $url);
    }
}

function _print_r($arr)
{
    echo '<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />';
    echo '<pre>';
    print_r($arr);
    echo '<pre>';
    exit;
}

function date_th($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}

function datetime_th($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear, $strHour:$strMinute น.";
}


function member_status($data)
{

    $status = "";

    if ($data == "ADMIN") {
        $status = "ผู้ดูแลระบบ";
    } elseif ($data == "DOCTOR") {
        $status = "แพทย์";
    } elseif ($data == "USER") {
        $status = "ผู้ป่วย";
    }

    return $status;

}


function mount_name($num)
{
    if ($num == 1) {
        $status = 'มกราคม';
    } elseif ($num == 2) {
        $status = 'กุมภาพันธ์';
    } elseif ($num == 3) {
        $status = 'มีนาคม';
    } elseif ($num == 4) {
        $status = 'เมษายน';
    } elseif ($num == 5) {
        $status = 'พฤษภาคม';
    } elseif ($num == 6) {
        $status = 'มิถุนายน';
    } elseif ($num == 7) {
        $status = 'กรกฎาคม';
    } elseif ($num == 8) {
        $status = 'สิงหาคม';
    } elseif ($num == 9) {
        $status = 'กันยายน';
    } elseif ($num == 10) {
        $status = 'ตุลาคม';
    } elseif ($num == 11) {
        $status = 'พฤศจิกายน';
    } elseif ($num == 12) {
        $status = 'ธันวาคม';
    }

    return $status;
}


function insulin_status($data)
{

    $status = "";

    if ($data == 0) {
        $status = "ไม่ฉีด";
    } elseif ($data == 1) {
        $status = "ฉีด";
    }

    return $status;

}

function duration_name($duration_id)
{
    $sql = "SELECT * FROM duration WHERE duration_id = '{$duration_id}'";
    $row = row_array($sql);

    return $row['duration_name'];
}

function statuss($num)
{
    $status = "";

    if ($num == 1) {
        $status = "<i style='color: green;' class='fa fa-check'></i>";
    } else {
        $status = "<i style='color: red;' class='fa fa-times'></i>";
    }

    return $status;
}

function duraicon($numid)
{
    $dicon = "";

    if ($numid == 1 OR $numid == 2) {
        $dicon = "<i style='color: GreenYellow;' class='fa fa-coffee'></i>";
    } elseif ($numid == 3 OR $numid == 4) {
        $dicon = "<i style='color: orange;' class='fas fa-sun'></i>";
    } elseif ($numid == 5 OR $numid == 6) {
        $dicon = "<i style='color: darkblue;' class='fas fa-moon'></i>";
    } elseif ($numid == 7) {
        $dicon = "<i style='color: black;' class='fas fa-birthday-cake'></i>";
    } elseif ($numid == 8) {
        $dicon = "<i style='color: red;' class='fa fa-tint'></i>";
    } elseif ($numid == 9) {
        $dicon = "<i style='color: red;' class='fa fa-tint'></i> <i style='color: red;' class='fa fa-tint'></i>";
    } elseif ($numid == 10) {
        $dicon = "<i style='color: black;' class='fas fa-bed'></i>";
    } else {
        $dicon = "";
    }

    return $dicon;
}

function getAgeY($dd, $mm, $yy)
{
    $bdate = new DateTime($yy . '-' . $mm . '-' . $dd);
//    $dateOfBirth = "17-10-1985";
    $dateOfBirth = date_format($bdate, "d-m-Y");
    $today = date("Y-m-d");
    $diff = date_diff(date_create($dateOfBirth), date_create($today));

    return $diff->format('%y');
}

function getAgeYMD($dd, $mm, $yy)
{
    $bdate = new DateTime($yy . '-' . $mm . '-' . $dd);
//    $dateOfBirth = "17-10-1985";
    $dateOfBirth = date_format($bdate, "d-m-Y");
    $today = date("Y-m-d");
    $diff = date_diff(date_create($dateOfBirth), date_create($today));

    return $diff->format('%y ปี %m เดือน %d วัน');
}

function calTDD($age, $weight)
{
    if ($age <= 6) {
        $TDD = $weight * 0.4;
    } elseif ($age < 15) {
        $TDD = $weight * 0.7;
    } else {
        $TDD = $weight * 1;
    }
    return $TDD;
}

function calICR($itype, $age, $weight)
{
    if ($itype == 1) {
        $ICR = 500 / calTDD($age, $weight);
    } else {
        $ICR = 450 / calTDD($age, $weight);
    }
    return floor($ICR);
}

function calISF($itype, $age, $weight)
{
    if ($itype == 1) {
        $ISF = 1800 / calTDD($age, $weight);
    } else {
        $ISF = 1500 / calTDD($age, $weight);
    }
    return floor($ISF);
}

function calMaxMin($member_id, $start, $end, $duration_id)
{
    $max = 0;
    $min = 1000;

    $sql = "SELECT * FROM (
    (SELECT DATE(date_time) as dates , date_time as datetimes , duration_id , bloodsugar_number , bloodsugar_result as insulin_number , 0 as title , 1 as statuss , rISF as realInsulin  FROM bloodsugar WHERE member_id = '{$member_id}' AND duration_id = '{$duration_id}'  AND date_time BETWEEN '{$start}' AND '{$end}')
    UNION ALL
    (SELECT DATE(eat_datetime) as dates , eat_datetime as datetimes , duration_id , '-' as bloodsugar_number , insulin_number , eat_id as title , insulin_status as statuss , rinsulin as realInsulin  FROM eat WHERE member_id = '{$member_id}' AND duration_id = '{$duration_id}' AND eat_datetime BETWEEN '{$start}' AND '{$end}')
) results
ORDER BY  datetimes ASC";
    $query = result_array($sql);
    foreach ($query as $key => $row) {

        if ($row['bloodsugar_number'] > $max) {
            $max = $row['bloodsugar_number'];
        }
        if ($row['bloodsugar_number'] > 0) {
            if ($row['bloodsugar_number'] < $min) {
                $min = $row['bloodsugar_number'];
            }
        }
    }
    if ($min == 1000) {
        $min = 0;
    }
    $sql2 = "SELECT AVG(bloodsugar_number) as xbar , STDDEV(bloodsugar_number) as sd  FROM bloodsugar 
WHERE member_id = '{$member_id}' AND duration_id = '{$duration_id}'  AND date_time BETWEEN '{$start}' AND '{$end}' ORDER BY  date_time ASC";
    $q2 = row_array($sql2);

//    return array($min, $max, $q2['xbar'], $q2['sd']);
    return array($min, $max, round($q2['xbar'], 2) , round($q2['sd'], 2));
}

function count_INW($member_id, $start, $end, $type)
{
    $count = 0;

    $sql = "SELECT * FROM (
    (SELECT DATE(date_time) as dates , date_time as datetimes , duration_id , bloodsugar_number , bloodsugar_result as insulin_number , 0 as title , 1 as statuss , rISF as realInsulin  FROM bloodsugar WHERE member_id = '{$member_id}' AND date_time BETWEEN '{$start}' AND '{$end}')
    UNION ALL
    (SELECT DATE(eat_datetime) as dates , eat_datetime as datetimes , duration_id , '-' as bloodsugar_number , insulin_number , eat_id as title , insulin_status as statuss , rinsulin as realInsulin  FROM eat WHERE member_id = '{$member_id}' AND eat_datetime BETWEEN '{$start}' AND '{$end}')
) results
ORDER BY  datetimes ASC";
    $query = result_array($sql);
    foreach ($query as $key => $row) {
        if ($row['bloodsugar_number'] > 0) {

            if ($type == "hypo") {
                if ($row['bloodsugar_number'] < 70) {
                    $count++;
                }
            } elseif ($type == "hyper") {
                if ($row['bloodsugar_number'] > 180) {
                    $count++;
                }
            }
        }
        if ($type == "insh") {
            if ($row['insulin_number'] > 0) {
                $count++;
            }
        }
        if ($type == "inre") {
            if ($row['realInsulin'] > 0) {
                $count++;
            }
        }
        if ($type == "inac") {
            if ($row['insulin_number'] > 0 && $row['realInsulin'] > 0) {
                if ($row['insulin_number'] == $row['realInsulin']) {
                    $count++;
                }
            }

        }
    }
    return $count;

}

function wtf($blahblah)
{
    $var1 = "ONe";
    $var2 = "tWo";

    if ($blahblah == "both") {
        return array($var1, $var2);
    }
}
?>






