<?php


function addtocart_food($data, $qty) {
    $pid = $data['food_id'];
    $name = $data['food_name'];
    $carb = $data['food_gram_carb'];


    if (is_array($_SESSION['cart_food'])) {
        if (check_item($pid, $qty)) {
            return;
        }

        $max = count($_SESSION['cart_food']);
        $_SESSION['cart_food'][$max]['pid'] = $pid;
        $_SESSION['cart_food'][$max]['qty'] = $qty;
        $_SESSION['cart_food'][$max]['name'] = $name;
        $_SESSION['cart_food'][$max]['carb'] = $carb;
    } else {
        $_SESSION['cart_food'] = array();
        $_SESSION['cart_food'][0]['pid'] = $pid;
        $_SESSION['cart_food'][0]['qty'] = $qty;
        $_SESSION['cart_food'][0]['name'] = $name;
        $_SESSION['cart_food'][0]['carb'] = $carb;
    }
}

function remove_food($pid) {
    $pid = intval($pid);
    $max = count($_SESSION['cart_food']);
    for ($i = 0; $i < $max; $i++) {
        if ($pid == $_SESSION['cart_food'][$i]['pid']) {
            unset($_SESSION['cart_food'][$i]);
            break;
        }
    }
    $_SESSION['cart_food'] = array_values($_SESSION['cart_food']);
}

function check_item($pid, $qty) {
    $pid = intval($pid);
    $max = count($_SESSION['cart_food']);
    $check = 0;

    for ($i = 0; $i < $max; $i++) {
        if ($pid == $_SESSION['cart_food'][$i]['pid']) {
            $_SESSION['cart_food'][$i]['qty'] = $qty;
            $check = 1;
            break;
        }
    }
    return $check;
}
?>

