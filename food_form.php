<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">จัดการอาหาร</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        ฟอร์มจัดการอาหาร
                    </div>

                    <?PHP
                    $id = "";
                    $food_name = "";
                    $food_gram_carb = "";
                   

                    if(isset($_GET['id'])){
                        $id = $_GET['id'];

                        $sql = "select * from food WHERE food_id = '{$id}'";
                        $query = row_array($sql);

                        $food_name = $query['food_name'];
                        $food_gram_carb = $query['food_gram_carb'];

                    }



                    ?>

                    <div class="panel-body">
                        <form role="form" action="process/food_process.php" method="post" enctype="multipart/form-data">

                            <input type="hidden" name="id" value="<?= $id ?>">


                            <div class="form-group">
                                <label>ชื่อ *</label>
                                <input class="form-control" type="text" name="name" maxlength="30" value="<?= $food_name; ?>"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>ปริมานคาร์โบไฮเดรต *</label>
                                <input class="form-control numberOnly" type="text" name="food_gram_carb"
                                       maxlength="5" type="text" name="tel"
                                       value="<?= $food_gram_carb; ?>" required>
                            </div>



                            <center>
                                <button type="submit" class="btn btn-success">บันทึก</button>
                                <button type="reset" class="btn btn-warning">รีเซต</button>
                            </center>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>




</body>
</html>
