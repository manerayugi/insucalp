<?PHP include 'config/database.php'; ?>
<?PHP include 'config/fnc.php'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
      integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>ตารางรายงานผล</title>
    <link href="assets/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box
        }

        .a4 {
            height: auto;
            width: 250mm;
            min-height: 220mm;
            margin: 20px auto 20px auto;
            border: 1px solid #f1f1e3;
            padding: 20px 40px;
            line-height: 20px;
            overflow: hidden;
        }

        h1 {
            text-align: center;
            font-size: 18px;
            font-weight: bold;
            padding-top: 20px;
            line-height: 30px;
        }

        h2 {
            text-align: center;
            font-size: 14px;
            font-weight: bold;
            padding-bottom: 2px;
            line-height: 30px;
            font-weight: normal;
        }

        span {
            padding: 0 50px 0 15px;
            border-bottom: 1px dashed #000;
        }

        .sen {
            width: 300px;
            height: 75px;
            font-size: 12px;
            text-align: center;
            line-height: 18px;
            padding-top: 20px;
            clear: both;
        }

        .bor_titel {
            width: 60%;
            border: 2px solid #000;
            border-radius: 20px;
            padding: 20px 10px;
            margin-bottom: 30px;
        }

        table.tb_title {
            width: 100%;
            height: 80px;
        }

        @media print {
            .a4 {
                border: none;
            }

            .print {
                display: none;
            }

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }

            .col-sm-12 {
                width: 100%;
            }

            .col-sm-11 {
                width: 91.66666667%;
            }

            .col-sm-10 {
                width: 83.33333333%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666667%;
            }

            .col-sm-7 {
                width: 58.33333333%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666667%;
            }

            .col-sm-4 {
                width: 33.33333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.66666667%;
            }

            .col-sm-1 {
                width: 8.33333333%;
            }

            .col-sm-pull-12 {
                right: 100%;
            }

            .col-sm-pull-11 {
                right: 91.66666667%;
            }

            .col-sm-pull-10 {
                right: 83.33333333%;
            }

            .col-sm-pull-9 {
                right: 75%;
            }

            .col-sm-pull-8 {
                right: 66.66666667%;
            }

            .col-sm-pull-7 {
                right: 58.33333333%;
            }

            .col-sm-pull-6 {
                right: 50%;
            }

            .col-sm-pull-5 {
                right: 41.66666667%;
            }

            .col-sm-pull-4 {
                right: 33.33333333%;
            }

            .col-sm-pull-3 {
                right: 25%;
            }

            .col-sm-pull-2 {
                right: 16.66666667%;
            }

            .col-sm-pull-1 {
                right: 8.33333333%;
            }

            .col-sm-pull-0 {
                right: auto;
            }

            .col-sm-push-12 {
                left: 100%;
            }

            .col-sm-push-11 {
                left: 91.66666667%;
            }

            .col-sm-push-10 {
                left: 83.33333333%;
            }

            .col-sm-push-9 {
                left: 75%;
            }

            .col-sm-push-8 {
                left: 66.66666667%;
            }

            .col-sm-push-7 {
                left: 58.33333333%;
            }

            .col-sm-push-6 {
                left: 50%;
            }

            .col-sm-push-5 {
                left: 41.66666667%;
            }

            .col-sm-push-4 {
                left: 33.33333333%;
            }

            .col-sm-push-3 {
                left: 25%;
            }

            .col-sm-push-2 {
                left: 16.66666667%;
            }

            .col-sm-push-1 {
                left: 8.33333333%;
            }

            .col-sm-push-0 {
                left: auto;
            }

            .col-sm-offset-12 {
                margin-left: 100%;
            }

            .col-sm-offset-11 {
                margin-left: 91.66666667%;
            }

            .col-sm-offset-10 {
                margin-left: 83.33333333%;
            }

            .col-sm-offset-9 {
                margin-left: 75%;
            }

            .col-sm-offset-8 {
                margin-left: 66.66666667%;
            }

            .col-sm-offset-7 {
                margin-left: 58.33333333%;
            }

            .col-sm-offset-6 {
                margin-left: 50%;
            }

            .col-sm-offset-5 {
                margin-left: 41.66666667%;
            }

            .col-sm-offset-4 {
                margin-left: 33.33333333%;
            }

            .col-sm-offset-3 {
                margin-left: 25%;
            }

            .col-sm-offset-2 {
                margin-left: 16.66666667%;
            }

            .col-sm-offset-1 {
                margin-left: 8.33333333%;
            }

            .col-sm-offset-0 {
                margin-left: 0%;
            }

            .visible-xs {
                display: none !important;
            }

            .hidden-xs {
                display: block !important;
            }

            table.hidden-xs {
                display: table;
            }

            tr.hidden-xs {
                display: table-row !important;
            }

            th.hidden-xs,
            td.hidden-xs {
                display: table-cell !important;
            }

            .hidden-xs.hidden-print {
                display: none !important;
            }

            .hidden-sm {
                display: none !important;
            }

            .visible-sm {
                display: block !important;
            }

            table.visible-sm {
                display: table;
            }

            tr.visible-sm {
                display: table-row !important;
            }

            th.visible-sm,
            td.visible-sm {
                display: table-cell !important;
            }
        }

    </style>
</head>

<body>

<?PHP
extract($_GET);
$sql = "SELECT * FROM member a INNER JOIN information b ON a.member_id = b.member_id WHERE a.member_id = '{$member_id}'";
$row = row_array($sql);
?>


<p class="text-center print" style="padding: 20px;">
    <button onclick="printDiv('printR')" type="button" class="btn btn-primary">
        Print สรุป
    </button>
    <button onclick="return window.print();" type="button" class="btn btn-primary">
        Print
    </button>
</p>

<div class="a4">
    <div id="printR">
        <center><img src="assets/img/logo.png" style="width: auto; height: 60px;" alt=""></center>
        <div class="form-group">
            <div class="col-sm-3">
                <h1>คุณ : <?= $row['member_name']; ?></h1>
            </div>
            <div class="col-sm-3">
                <h1>อายุ : <?PHP echo getAgeY($row['bday'], $row['bmount'], $row['byear']); ?> ปี</h1>
            </div>
            <div class="col-sm-3">
                <h1>เพศ : <?PHP if ($row['member_sex'] == 'M') {
                        echo "ชาย";
                    } else {
                        echo "หญิง";
                    }; ?></h1>
            </div>
            <div class="col-sm-3">
                <h1>โทร. : <?= $row['member_tel']; ?></h1>
            </div>
        </div>
        <h2>ปริมาณอินซูลินต่อกรัมของคาร์โบไฮเดรต : <i style='color: GreenYellow;' class='fa fa-coffee'></i>เช้า
            1:<?= $row['ICR_br']; ?>
            <i style='color: orange;' class='fas fa-sun'></i>กลางวัน 1:<?= $row['ICR_br']; ?>
            <i style='color: darkblue;' class='fas fa-moon'></i>เย็น 1:<?= $row['ICR_br']; ?></h2>
        <h2>ปริมาณอินซูลินสำหรับการแก้ไขภาวะน้ำตาลในเลือดสูง : <i style='color: GreenYellow;' class='fa fa-coffee'></i>เช้า
            1:<?= $row['ISF_br']; ?>
            <i style='color: orange;' class='fas fa-sun'></i>กลางวัน 1:<?= $row['ISF_br']; ?>
            <i style='color: darkblue;' class='fas fa-moon'></i>เย็น 1:<?= $row['ISF_br']; ?></h2>
        <h2>ระดับน้ำตาลในเลือดเป้าหมาย : <i style='color: red;' class='fa fa-tint'></i> <?= $row['MAX_blsu']; ?> mg/dL
        </h2>
        <div class="form-group">
            <div class="col-sm-4">
                <h2>แนะนำการฉีดจำนวน : <?= count_INW($member_id, $start, $end, insh); ?> ครั้ง</h2>
            </div>
            <div class="col-sm-4">
                <h2>ฉีดจริงจำนวน : <?= count_INW($member_id, $start, $end, inre); ?> ครั้ง</h2>
            </div>
            <div class="col-sm-4">
                <h2>ทำตามคำแนะนำจำนวน : <?= count_INW($member_id, $start, $end, inac); ?> ครั้ง</h2>
            </div>
        </div>
        <h1><i style='color: red;' class='fa fa-tint'></i><i style='color: red;' class='fa fa-tint'></i>
            ระดับน้ำตาลในเลือด ต่ำสุด-สูงสุด ตามช่วงเวลาต่างๆ(mg/dL)
            <i style='color: red;' class='fa fa-tint'></i><i style='color: red;' class='fa fa-tint'></i></h1>
        <table class="table table-bordered table-striped">
            <thead>
            <tr bgcolor="LightBlue">
                <th style="vertical-align: middle;" rowspan="2" width="150" class="text-center">ช่วง</th>
                <th colspan="3" width="150" class="text-center">ก่อนอาหาร</th>
                <th colspan="3" width="150" class="text-center">หลังอาหาร</th>
            </tr>
            <tr bgcolor="LightBlue">
                <!--            <th width="150" class="text-center">ช่วง</th>-->
                <th width="150" class="text-center">ต่ำสุด-สูงสุด</th>
                <th width="150" class="text-center">ค่าเฉลี่ย</th>
                <th width="150" class="text-center">ส่วนเบี่ยงเบนมาตรฐาน</th>
                <th width="150" class="text-center">ต่ำสุด-สูงสุด</th>
                <th width="150" class="text-center">ค่าเฉลี่ย</th>
                <th width="150" class="text-center">ส่วนเบี่ยงเบนมาตรฐาน</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-center">เช้า</td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 1)[0]; ?>
                    - <?= calMaxMin($member_id, $start, $end, 1)[1]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 1)[2]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 1)[3]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 2)[0]; ?>
                    - <?= calMaxMin($member_id, $start, $end, 2)[1]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 2)[2]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 2)[3]; ?></td>
            </tr>
            <tr>
                <td class="text-center">กลางวัน</td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 3)[0]; ?>
                    - <?= calMaxMin($member_id, $start, $end, 3)[1]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 3)[2]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 3)[3]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 4)[0]; ?>
                    - <?= calMaxMin($member_id, $start, $end, 4)[1]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 4)[2]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 4)[3]; ?></td>
            </tr>
            <tr>
                <td class="text-center">เย็น</td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 5)[0]; ?>
                    - <?= calMaxMin($member_id, $start, $end, 5)[1]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 5)[2]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 5)[3]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 6)[0]; ?>
                    - <?= calMaxMin($member_id, $start, $end, 6)[1]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 6)[2]; ?></td>
                <td class="text-center"><?= calMaxMin($member_id, $start, $end, 6)[3]; ?></td>
            </tr>
            </tbody>
        </table>
        <div class="form-group">
            <div class="col-sm-6">
                <h1>เกิดภาวะน้ำตาลต่ำจำนวน : <?= count_INW($member_id, $start, $end, hypo); ?> ครั้ง</h1>
            </div>
            <div class="col-sm-6">
                <h1>เกิดภาวะน้ำตาลสูงจำนวน : <?= count_INW($member_id, $start, $end, hyper); ?> ครั้ง</h1>
            </div>
        </div>


        <?PHP

        $sql = "SELECT * FROM (
    (SELECT DATE(date_time) as dates , date_time as datetimes , duration_id , bloodsugar_number , bloodsugar_result as insulin_number , 0 as title , 1 as statuss , rISF as realInsulin  FROM bloodsugar WHERE member_id = '{$member_id}'AND date_time BETWEEN '{$start}' AND '{$end}' AND bloodsugar_number < 70)
    UNION ALL
    (SELECT DATE(date_time) as dates , date_time as datetimes , duration_id , bloodsugar_number , bloodsugar_result as insulin_number , 0 as title , 1 as statuss , rISF as realInsulin  FROM bloodsugar WHERE member_id = '{$member_id}'AND date_time BETWEEN '{$start}' AND '{$end}' AND bloodsugar_number > 180)
) results
ORDER BY  datetimes ASC";

        $query = result_array($sql);

        ?>
        <table class="table table-bordered table-striped">
            <thead>
            <tr bgcolor="LightBlue">
                <th style="vertical-align: middle;" rowspan="2" width="200" class="text-center">วันที่</th>
                <th style="vertical-align: middle;" rowspan="2" width="200" class="text-center">เหตุการณ์</th>
                <th style="vertical-align: middle;" rowspan="2" width="150" class="text-center">
                    ระดับน้ำตาลในเลือด(mg/dL)
                </th>
                <!--                <th class="text-center" width="200" class="text-center">วันที่</th>-->
                <!--                <th width="230" class="text-center">เหตุการณ์</th>-->
                <!--                <th width="150" class="text-center">ระดับน้ำตาลในเลือด</th>-->
                <th colspan="2" width="100" class="text-center">อินซูลิน(Unit)</th>
            </tr>
            <tr bgcolor="LightBlue">
                <th width="100" class="text-center">แนะนำ</th>
                <th width="100" class="text-center">ฉีดจริง</th>
            </tr>
            </thead>
            <tbody>
            <?PHP foreach ($query as $key => $row) { ?>
                <tr>
                    <td class="text-center"><?= $row['datetimes']; ?></td>
                    <td class="text-center"><? if ($row['duration_id'] == 8 OR $row['duration_id'] == 9) {
                            echo duration_name($row['duration_id']);
                        } else {
                            if ($row['bloodsugar_number'] > 180) {
                                echo "ภาวะน้ำตาลสูง " . duration_name($row['duration_id']);
                            } else {
                                echo "ภาวะน้ำตาลต่ำ " . duration_name($row['duration_id']);
                            }
                        } ?></td>
                    <td class="text-center"><?= $row['bloodsugar_number'] . duraicon($row['duration_id']); ?></td>
                    <td class="text-center">
                        <?= $row['insulin_number']; ?>
                        <? //= statuss($row['statuss']); ?>
                    </td>
                    <td class="text-center">
                        <?= $row['realInsulin']; ?>
                        <? //= statuss($row['statuss']); ?>
                    </td>

                </tr>
            <?PHP } ?>
            </tbody>
        </table>


    </div>

    <h1>รายละเอียด</h1>

    <?PHP

    $sql = "SELECT * FROM (
    (SELECT DATE(date_time) as dates , date_time as datetimes , duration_id , bloodsugar_number , bloodsugar_result as insulin_number , 0 as title , 1 as statuss , rISF as realInsulin  FROM bloodsugar WHERE member_id = '{$member_id}'AND date_time BETWEEN '{$start}' AND '{$end}')
    UNION ALL
    (SELECT DATE(eat_datetime) as dates , eat_datetime as datetimes , duration_id , '-' as bloodsugar_number , insulin_number , eat_id as title , insulin_status as statuss , rinsulin as realInsulin  FROM eat WHERE member_id = '{$member_id}'AND eat_datetime BETWEEN '{$start}' AND '{$end}')
) results
ORDER BY  datetimes ASC";

    $query = result_array($sql);

    ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr bgcolor="LightBlue">
            <th class="text-center" width="200" class="text-center">วันที่</th>
            <th width="230" class="text-center">ช่วง</th>
            <th width="150" class="text-center">น้ำตาล ก่อน</th>
            <th width="150" class="text-center">น้ำตาล หลัง</th>
            <th width="200" class="text-center">อินซูลิน ที่แนะนำ</th>
            <th width="200" class="text-center">อินซูลิน ที่ฉีดจริง</th>
            <th width="430" class="text-center">รายการ</th>
        </tr>
        </thead>
        <tbody>
        <?PHP foreach ($query as $key => $row) { ?>
            <tr>
                <td class="text-center"><?= $row['dates']; ?></td>
                <td class="text-center"><?= duration_name($row['duration_id']); ?></td>
                <td class="text-center"><? if ($row['duration_id'] == 2 OR
                        $row['duration_id'] == 4 OR
                        $row['duration_id'] == 6) {
                        echo "-";
                    } else {
                        echo $row['bloodsugar_number'] . " ";
                        if ($row['bloodsugar_number'] > 0) {
                            echo duraicon($row['duration_id']);
                        }
                    } ?> </td>
                <td class="text-center"><? if ($row['duration_id'] == 2 OR
                        $row['duration_id'] == 4 OR
                        $row['duration_id'] == 6) {
                        echo $row['bloodsugar_number'] . " ";
                        if ($row['bloodsugar_number'] > 0) {
                            echo duraicon($row['duration_id']);
                        }
                    } else {
                        echo "-";
                    } ?></td>
                <td class="text-center">
                    <?= $row['insulin_number']; ?>
                    <? //= statuss($row['statuss']); ?>
                </td>
                <td class="text-center">
                    <?= $row['realInsulin']; ?>
                    <? //= statuss($row['statuss']); ?>
                </td>
                <td class="text-left">
                    <?PHP if ($row['title'] == 0) { ?>
                        <b> บันทึกระดับน้ำตาลในเลือด</b>

                        <?PHP if ($row['duration_id'] > 6) { ?>
                            <ul style="font-size: 12px; list-style: none; padding: 0;">
                                <li>- เวลาบันทึก : <?= $row['datetimes']; ?></li>
                            </ul>
                        <?PHP } ?>

                    <?PHP } else { ?>
                        <?PHP
                        $sql = "SELECT * FROM eat WHERE eat_id = '{$row['title']}'";
                        $eat = row_array($sql);
                        ?>

                        <b> บันทึกการทานอาหาร <?= $eat['eat_carb']; ?> g ดังนี้ </b>
                        <?PHP
                        $sql = "SELECT * FROM eat_detail a INNER JOIN food b ON a.food_id = b.food_id WHERE eat_id = '{$row['title']}'";
                        $cc = result_array($sql);
                        ?>

                        <ul style="font-size: 12px; list-style: none; padding: 0;">
                            <?PHP foreach ($cc as $_cc) { ?>
                                <li>- <?= $_cc['food_name']; ?> : <?= $_cc['eat_detail_carb']; ?> g, <?= $_cc['eat_detail_qty']; ?> หน่วย รวม <?= $_cc['eat_detail_qty']*$_cc['eat_detail_carb']; ?> g</li>
                            <?PHP } ?>
                        </ul>
                    <?PHP } ?>

                </td>
            </tr>
        <?PHP } ?>
        </tbody>
    </table>

</div>

</body>
<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;
        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</html>