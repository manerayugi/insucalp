<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">


        <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        ผู้จัดทำ
                    </div>
                    <div class="panel-body" style="min-height: 330px; overflow: hidden;">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-md-3">
                                    <img src="assets/img/1.JPG" class="img-responsive" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h4>นายพิทยาธร รินแก้วงาม</h4>
                                    560610564
                                    <br>
                                    ภาควิชาวิศวกรรมคอมพิวเตอร์ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่

                                </div>
                            </div>
                            <hr>
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-md-3">
                                    <img src="assets/img/2.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h4>อาจารย์ ดร.ปฏิเวธ วุฒิสารวัฒนา</h4>
                                    ภาควิชาวิศวกรรมคอมพิวเตอร์ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่

                                </div>
                            </div>
                            <hr>
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-md-3">
                                    <img src="assets/img/3.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h4>ผู้ช่วยศาสตราจารย์ แพทย์หญิง ประไพ เดชคำรณ</h4>
                                    ภาควิชากุมารเวชศาสตร์ คณะแพทยศาสตร์ มหาวิทยาลัยเชียงใหม่

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
