<?php

include '../config/database.php';
include '../config/fnc.php';


extract($_POST);

if(empty($id)){
    $sql = "select * from member where member_username = '{$username}'";
    $row = row_array($sql);

    if($row){
        echo "<meta charset='utf-8'/><script>alert('ชื่อผู้ใช้งานนี้มีในระบบเรียบร้อยแล้ว!!');window.history.back();</script>";
        die();
    }

    $sql = "select * from member where member_tel = '{$tel}'";
    $row = row_array($sql);

    if($row){
        echo "<meta charset='utf-8'/><script>alert('เบอร์โทรนี้มีในระบบเรียบร้อยแล้ว!!');window.history.back();</script>";
        die();
    }

    $sql = "select * from member where member_email = '{$email}'";
    $row = row_array($sql);

    if($row){
        echo "<meta charset='utf-8'/><script>alert('อีเมล์นี้มีในระบบเรียบร้อยแล้ว!!');window.history.back();</script>";
        die();
    }
	if($password != $repassword){
		echo "<meta charset='utf-8'/><script>alert('Password not Match!!!');window.history.back();</script>";
        die();
	}


    $data = array(
        "member_name" => $name,
        "member_username" => $username,
        "member_password" => $password,
        "member_tel" => $tel,
        "member_sex" => $sex,
        "member_insulinRec" => $insulinRec,
        "member_email" => $email,
        "member_status" => "USER",
    );

    insert("member", $data);


    echo "<meta charset='utf-8'/><script>alert('เพิ่มข้อมูลสำเร็จ!!');location.href='../user.php';</script>";
}else{
    $sql = "select * from member where member_username = '{$username}' AND member_id != '{$id}'";
    $row = row_array($sql);

    if($row){
        echo "<meta charset='utf-8'/><script>alert('ชื่อผู้ใช้งานนี้มีในระบบเรียบร้อยแล้ว!!');window.history.back();</script>";
        die();
    }

    $sql = "select * from member where member_tel = '{$tel}' AND member_id != '{$id}'";
    $row = row_array($sql);

    if($row){
        echo "<meta charset='utf-8'/><script>alert('เบอร์โทรนี้มีในระบบเรียบร้อยแล้ว!!');window.history.back();</script>";
        die();
    }

    $sql = "select * from member where member_email = '{$email}' AND member_id != '{$id}'";
    $row = row_array($sql);

    if($row){
        echo "<meta charset='utf-8'/><script>alert('อีเมล์นี้มีในระบบเรียบร้อยแล้ว!!');window.history.back();</script>";
        die();
    }


    $data = array(
        "member_name" => $name,
        "member_username" => $username,
        "member_password" => $password,
        "member_tel" => $tel,
        "member_sex" => $sex,
        "member_insulinRec" => $insulinRec,
        "member_email" => $email
    );

    update("member", $data,"member_id = {$id}");

    echo "<meta charset='utf-8'/><script>alert('แก้ไขข้อมูลสำเร็จ!!');location.href='../user.php';</script>";
}



?>

