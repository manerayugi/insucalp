<?php

include '../config/database.php';
include '../config/fnc.php';

extract($_POST);

$hour = date("H");
$date = date('Y-m-d H:i:s');
$total = check_session('cart_food_sum');

if ($duration_id > 6) {
    $sdt = "{$s_date} {$s_time}";
}else{

    $s_time1 = $_POST['thr'].":".$_POST['tmin'].":00";
    $sdt = "{$s_date} {$s_time1}";
}


if ($cart_food = $_SESSION['cart_food']) {


    $data = array(
        "eat_datetime" => $sdt,
        "insulin_number" => $insulin_number,
        "insulin_status" => $insulin_status,
        "duration_id" => $duration_id,
        "member_id" => $member_id,
        "eat_carb" => $total,
        "rinsulin" => $rinsulin
    );
    if ($rinsulin == 0){
        $insulin_status = 0;
    }else{
        $insulin_status = 1;
    }
    $data = array(
        "eat_datetime" => $sdt,
        "insulin_number" => $insulin_number,
        "insulin_status" => $insulin_status,
        "duration_id" => $duration_id,
        "member_id" => $member_id,
        "eat_carb" => $total,
        "rinsulin" => $rinsulin
    );
    insert("eat", $data);

    $max = row_array('SELECT MAX(eat_id) as max FROM eat;');
    $id = $max['max'];

    foreach ($cart_food as $_cart) {

        $arr2 = array(
            "eat_detail_qty" => $_cart['qty'],
            "eat_detail_carb" => $_cart['carb'],
            "eat_id" => $id,
            "food_id" => $_cart['pid']
        );

        insert("eat_detail", $arr2);

    }

    unset($_SESSION['cart_food']);
    unset($_SESSION['cart_food_sum']);

    echo "<meta charset='utf-8'/><script>alert('ทำรายการสำเร็จ!!');location.href='../save_food.php';</script>";
} else {
    echo "<meta charset='utf-8'/><script>alert('ไม่พบอาหารที่คุณเลือก!!');location.href='../save_food_form.php';</script>";
}

?>
