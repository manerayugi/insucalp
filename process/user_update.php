<?php

include '../config/database.php';
include '../config/fnc.php';


extract($_POST);

if ($check) {

    $data = array(
        "bday" => $bday,
        "bmount" => $bmount,
        "byear" => $byear,
        "weight" => $weight,
        "itype" => $itype
    );
    $age = getAgeY($bday, $bmount, $byear);
    $ICR = calICR($itype, $age, $weight);
    $ISF = calISF($itype, $age, $weight);

if ($member_insulinRec == 2){
    $data = array(
        "age" => $age,
        "weight" => $weight,
        "height" => $height,
        "ICR_br" => $ICR_br,
        "ICR_lu" => $ICR_lu,
        "ICR_di" => $ICR_di,
        "ISF_br" => $ISF_br,
        "ISF_lu" => $ISF_lu,
        "ISF_di" => $ISF_di,
        "MAX_blsu" => $MAX_blsu,
        "doctor" => $doctor,
        "member_id" => $id,
        "bday" => $bday,
        "bmount" => $bmount,
        "byear" => $byear,
        "itype" => $itype
    );
}else{
    $data = array(
        "age" => $age,
        "weight" => $weight,
        "height" => $height,
        "ICR_br" => $ICR,
        "ICR_lu" => $ICR,
        "ICR_di" => $ICR,
        "ISF_br" => $ISF,
        "ISF_lu" => $ISF,
        "ISF_di" => $ISF,
        "MAX_blsu" => $MAX_blsu,
        "doctor" => $doctor,
        "member_id" => $id,
        "bday" => $bday,
        "bmount" => $bmount,
        "byear" => $byear,
        "itype" => $itype
    );
}



    insert("information", $data);


    echo "<meta charset='utf-8'/><script>alert('อัพเดทข้อมูลสำเร็จ!!');location.href='../{$url}';</script>";

} else {
    $data = array(
        "bday" => $bday,
        "bmount" => $bmount,
        "byear" => $byear,
        "weight" => $weight,
        "itype" => $itype
    );
    $age = getAgeY($bday, $bmount, $byear);
    $ICR = calICR($itype, $age, $weight);
    $ISF = calISF($itype, $age, $weight);

if ($member_insulinRec == 2) {
    $data = array(
        "age" => $age,
        "weight" => $weight,
        "height" => $height,
        "ICR_br" => $ICR_br,
        "ICR_lu" => $ICR_lu,
        "ICR_di" => $ICR_di,
        "ISF_br" => $ISF_br,
        "ISF_lu" => $ISF_lu,
        "ISF_di" => $ISF_di,
        "MAX_blsu" => $MAX_blsu,
        "doctor" => $doctor,
        "bday" => $bday,
        "bmount" => $bmount,
        "byear" => $byear,
        "itype" => $itype,
        "tbrs" => $tbrs,
        "tbre" => $tbre,
        "tlus" => $tlus,
        "tlue" => $tlue,
        "tdis" => $tdis,
        "tdie" => $tdie
    );
}else{
    $data = array(
        "age" => $age,
        "weight" => $weight,
        "height" => $height,
        "ICR_br" => $ICR,
        "ICR_lu" => $ICR,
        "ICR_di" => $ICR,
        "ISF_br" => $ISF,
        "ISF_lu" => $ISF,
        "ISF_di" => $ISF,
        "MAX_blsu" => $MAX_blsu,
        "doctor" => $doctor,
        "bday" => $bday,
        "bmount" => $bmount,
        "byear" => $byear,
        "itype" => $itype,
        "tbrs" => $tbrs,
        "tbre" => $tbre,
        "tlus" => $tlus,
        "tlue" => $tlue,
        "tdis" => $tdis,
        "tdie" => $tdie
    );
}
    update("information", $data, "member_id = '{$id}'");


    echo "<meta charset='utf-8'/><script>alert('อัพเดทข้อมูลสำเร็จ!!');location.href='../{$url}';</script>";
}


?>

