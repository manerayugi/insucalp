<?php
/**
 * Created by PhpStorm.
 * User: Phantom OF Time
 * Date: 7/18/2018
 * Time: 11:51 PM
 */
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
    <link rel="stylesheet" href="assets/js/time/jquery.ui.timepicker.css"/>
    <link rel="stylesheet" href="assets/js/time/ui-lightness/jquery-ui-1.10.0.custom.min.css"/>
    <script src="assets/js/time/jquery.ui.timepicker.js"></script>
    <style>
        div.ui-datepicker {
            font-size: 14px;
        }

        .ui-datepicker-month, .ui-datepicker-year {
            color: red;
        }
    </style>
    <script>
        $(document).ready(function () {

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });


            $('.timepicker').timepicker({
                timeFormat: 'H:i:s',
                minTime: '11:00:12',
                maxTime: '11:33:33'

                //onHourShow: OnHourShowCallback
            });

            function OnHourShowCallback(hour) {
                return true; // valid
            }
        });
    </script>
</head>
<body>
<?PHP include 'include/menu.php';
extract($_POST);
?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">บันทึกระดับน้ำตาลในเลือด</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        บันทึกระดับน้ำตาลในเลือด
                    </div>

                    <?PHP

                    $member_id = check_session("member_id");

                    $sql = "SELECT * FROM duration WHERE duration_id = '{$duration_id}'";
                    $duration = row_array($sql);

                    $date_time = date("Y-m-d H:i:s");
                    $hour = date("H");

                    $sql = "SELECT * FROM information WHERE member_id = '{$member_id}'";
                    $information = row_array($sql);
                    $age = $information['age'];
                    $tbrs = $information['tbrs'];
                    $tbre = $information['tbre'];
                    $tlus = $information['tlus'];
                    $tlue = $information['tlue'];
                    $tdis = $information['tdis'];
                    $tdie = $information['tdie'];
                    $startHR = 0;
                    $endHR = 0;
                    if ($duration['duration_id'] == 1 OR $duration['duration_id'] == 2) {
                        $startHR = $tbrs;
                        $endHR = $tbre;
                    } elseif ($duration['duration_id'] == 3 OR $duration['duration_id'] == 4) {
                        $startHR = $tlus;
                        $endHR = $tlue;
                    } elseif ($duration['duration_id'] == 5 OR $duration['duration_id'] == 6) {
                        $startHR = $tdis;
                        $endHR = $tdie;
                    } else {
                        $startHR = 0;
                        $endHR = 0;
                    }

                    if ($age <= 5) {
                        $eat_carb = 5;
                    } elseif ($age < 18) {
                        $eat_carb = 15;
                    } else {
                        $eat_carb = 25;
                    }

                    if ($hour >= 6 && $hour < 12) {
                        $ISF_now = $information['ISF_br'];
                    } elseif ($hour >= 12 && $hour < 18) {
                        $ISF_now = $information['ISF_lu'];
                    } else {
                        $ISF_now = $information['ISF_di'];
                    }
                    $hyper = 0;
                    $hyper = floor(($bloodsugar_number - $information['MAX_blsu']) / $ISF_now);

                    if ($duration_id == 8) {
                        if ($bloodsugar_number >= 70) {
                            echo "<meta charset='utf-8'/><script>alert('ภาวะน้ำตาลต่ำห้ามเกิน 70 mg/dL!!');window.location.href = document.referrer;</script>";
                            die();
                        }
                    } elseif ($duration_id == 9) {
                        if ($bloodsugar_number <= 180) {
                            echo "<meta charset='utf-8'/><script>alert('ภาวะน้ำตาลสูงห้ามต่ำกว่า 180 mg/dL!!');window.location.href = document.referrer;</script>";
                            die();
                        }
                    }
                    ?>


                    <div class="panel-body">
                        <form action="process/bloodsugar_process.php" method="post">
                            <h3 class="text-center" style="font-size: 16px; line-height: 26px;">
                                <strong>ระดับน้ำตาลในเลือด : </strong><?= $bloodsugar_number; ?> mg/dL
                                <br>
                                <strong>ช่วงเวลา : </strong><?= $duration['duration_name']; ?>
                            </h3>
                            <hr>


                            <?PHP
                            $show = 0;
                            if ($bloodsugar_number < 70) { ?>
                                <center><b style="font-size: 40px; color: red;">น้ำตาลต่ำ!!!!!</b></center><br>
                                กรุณาทาน น้ำผลไม้ หรือน้ำหวาน หรือน้ำตาลโมเลกุลเดี่ยว เป็นจำนวน <?= $eat_carb; ?> กรัมของคาร์โบไฮเดรต จากนั้นรอ 15 นาทีแล้วเจาะซ้ำอีกครั้ง
                                หากน้ำตาลยังต่ำอยู่ให้ทานน้ำตาลโมเลกุลเดี่ยว เป็นจำนวน <?= $eat_carb; ?> กรัมของคาร์โบไฮเดรต อีกครั้งหนึ่ง รอ15นาทีแล้วเจาะเลือดซ้ำ
                                หากน้ำตาลมากกว่า 70 mg/dL แล้ว หากใกล้มื้ออาหารให้ทานอาหารเลย หากไม่ใกล้มื้ออาหาร ให้ทานน้ำตาลโมเลกุลคู่เช่น ข้าว ขนมปัง นม กล้วย
                                เป็นจำนวน  <?= $eat_carb; ?> กรัมของคาร์โบไฮเดรต
                            <?PHP } elseif ($duration_id == 10 && $bloodsugar_number < 100) {
                                if ($bloodsugar_number < 100) { ?>
                                    <center><b style="font-size: 30px; color: #FF3300;">เสี่ยงภาวะน้ำตาลในเลือดต่ำระหว่างนอนหลับ</b>
                                    </center><br>
                                    กรุณาทานน้ำตาลโมเลกุลคู่เช่น ข้าว ขนมปัง นม กล้วย เป็นจำนวน <?= $eat_carb; ?> กรัมของคาร์โบไฮเดรต เพื่อป้องกันภาวะน้ำตาลในเลือดต่ำระหว่างนอนหลับ
                                <?PHP }
                            } elseif ($bloodsugar_number <= 110) { ?>
                                <center><b style="font-size: 40px; color: green;">น้ำตาลอยู่ในเกณฑ์ปกติ</b></center><br>
                            <?PHP } elseif ($bloodsugar_number <= $information['MAX_blsu']) { ?>
                                <center><b style="font-size: 40px; color: darkgreen;">น้ำตาลอยู่ในเกณฑ์เป้าหมาย</b>
                                </center><br>
                            <?PHP } elseif ($bloodsugar_number < 180) { ?>
                                <center><b style="font-size: 40px; color: orange;">น้ำตาลอยู่ในเกณฑ์สูงกว่าเป้าหมายเล็กน้อย</b>
                                </center><br>
                            <?PHP } elseif ($bloodsugar_number >= 180) { ?>
                                <center><b style="font-size: 40px; color: red;">น้ำตาลสูง!!!!!</b></center><br>
                            <?PHP }  ?>

                            <div class="row">

                            </div>

                            <?PHP

                            $sql = "SELECT * FROM information WHERE member_id = '{$member_id}'";
                            $information = row_array($sql);
                            $now_time = "อินซูลินสำหรับแก้ไขภาวะน้ำตาลในเลือดสูง";
                            if ($duration_id == 1) {
                                $insulin_check = $information['ISF_br'];
                            } elseif ($duration_id == 3) {
                                $insulin_check = $information['ISF_lu'];
                            } elseif ($duration_id == 6) {
                                $insulin_check = $information['ISF_di'];
                            } else {
                                if ($hour >= 6 && $hour < 12) {
                                    $insulin_check = $information['ISF_br'];
                                } elseif ($hour >= 12 && $hour < 18) {
                                    $insulin_check = $information['ISF_lu'];
                                } else {
                                    $insulin_check = $information['ISF_di'];
                                }
                            }
                            if ($bloodsugar_number > $information['MAX_blsu']) {
                                $bloodsugar_result = floor(($bloodsugar_number - $information['MAX_blsu']) / $insulin_check);
                            } else {
                                $insulin_check = 0;
                                $bloodsugar_result = 0;
                            }


                            $s_date = date("Y-m-d");
                            $s_time = date("H:i:s");

                            if (isset($_GET['s_date'])) {
                                $s_date = $_GET['s_date'];
                            }
                            if (isset($_GET['s_time'])) {
                                $s_time = $_GET['s_time'];
                            }

                            ?>

                            <hr>
                            <h3 class="text-center" style="font-size: 16px; line-height: 26px;">
                                <?PHP if ($bloodsugar_result > 0) { ?>
                                    <b><?= $now_time; ?></b>
                                    <br>
                                    <b style="font-size: 20px; color: red;">
                                        กรุณาฉีดอินซูลิน <?= $bloodsugar_result; ?> Unit
                                        <?PHP $rinsulin = $bloodsugar_result; ?>
                                    </b><br>

                                    อินซูลินที่ฉีดจริง : <input type="text" name="rinsulin" maxlength="3"
                                                                value="<?= $rinsulin; ?>"> unit
                                    <br>
                                <?PHP } ?>
                            </h3>

                            <hr>
                            <div class="form-group" style=" text-align: center; margin-bottom: 20px; overflow: hidden;">
                                <label class="col-md-3 control-label"
                                       style="text-align: center; padding-top: 5px;">ช่วงเวลา *</label>

                                <div class="col-md-3" style="padding-top: 4px;">
                                    <input style="width: 100px" type="text" name="s_date" value="<?= $s_date; ?>"
                                           class="form-control datepicker" placeholder="วัน" required>
                                </div>
                                <?PHP
                                if ($startHR == 0 AND $endHR == 0) {
                                    ?>
                                    <div class="col-md-3" style="padding-top: 4px;">
                                        <input style="width: 100px" type="text" name="s_time" value="<?= $s_time; ?>"
                                               class="form-control timepicker" placeholder="เวลา" required>
                                    </div>
                                    <?PHP
                                } else {
                                    ?>
                                    <div class="col-md-3">
                                        <select name="thr" class="form-control" required>
                                            <option value="" >นาฬิกา</option>
                                            <?PHP $thr = 0;
                                            for ($thr = $startHR; $thr <= $endHR; $thr++) { ?>
                                                <option value="<?= ($thr <= 9 ? '0' . $thr : $thr) ?>"><?= ($thr <= 9 ? '0' . $thr : $thr) ?></option>
                                            <?PHP } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="tmin" class="form-control" required>
                                            <option value="" >นาที</option>
                                            <?PHP $tmin = 0;
                                            for ($tmin = 0; $tmin <= 60; $tmin++) { ?>
                                                <option value="<?= ($tmin <= 9 ? '0' . $tmin : $tmin) ?>"><?= ($tmin <= 9 ? '0' . $tmin : $tmin) ?></option>
                                            <?PHP } ?>
                                        </select>
                                    </div>

                                <?PHP } ?>

                            </div>


                            <hr>
                            <input type="hidden" name="member_id" value="<?= $member_id; ?>">
                            <input type="hidden" name="duration_id" value="<?= $duration_id; ?>">
                            <input type="hidden" name="bloodsugar_number" value="<?= $bloodsugar_number; ?>">
                            <input type="hidden" name="MAX_blsu_now" value="<?= $information['MAX_blsu']; ?>">
                            <input type="hidden" name="ISF_now" value="<?= $insulin_check; ?>">
                            <input type="hidden" name="bloodsugar_result" value="<?= $bloodsugar_result; ?>">




                            <hr>
                            <center>
                                <a href="save_glycemic.php"
                                   class="btn btn-warning">ย้อนกลับ</a>
                                <button type="submit" class="btn btn-success">บันทึกรายการ</button>
                            </center>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>

