<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">จัดการผู้ป่วย</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        ฟอร์มจัดการผู้ป่วย
                    </div>

                    <?PHP
                    $id = "";
                    $name = "";
                    $username = "";
                    $password = "";
                    $tel = "";
                    $email = "";
                    $sex = "M";

                    if(isset($_GET['id'])){
                        $id = $_GET['id'];

                        $sql = "select * from member WHERE member_id = '{$id}'";
                        $query = row_array($sql);

                        $name = $query['member_name'];
                        $username = $query['member_username'];
                        $password = $query['member_password'];
                        $tel = $query['member_tel'];
                        $email = $query['member_email'];
                        $sex = $query['member_sex'];

                    }



                    ?>

                    <div class="panel-body">
                        <form role="form" action="process/user_process.php" method="post" enctype="multipart/form-data">

                            <input type="hidden" name="id" value="<?= $id ?>">

                            <p>
                                <b>เพศ : </b>
                                <input type="radio" style="width: 20px;" name="sex" <?= $sex == "M" ? "checked" : "" ?>
                                       value="M"> ชาย ,
                                <input type="radio" style="width: 20px;" name="sex" <?= $sex == "F" ? "checked" : "" ?>
                                       value="F"> หญิง
                            </p>

                            <div class="form-group">
                                <label>ชื่อ *</label>
                                <input class="form-control" type="text" name="name" maxlength="30" value="<?= $name; ?>"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>ชื่อผู้ใช้งาน *</label>
                                <input class="form-control" type="text" name="username" maxlength="15"
                                       value="<?= $username; ?>" required>
                            </div>
                            <div class="form-group">
                                <label>รหัสผ่าน *</label>
                                <input class="form-control" type="password" name="password" maxlength="15"
                                       value="<?= $password; ?>" required>
                            </div>
                            <div class="form-group">
                                <label>เบอร์โทร *</label>
                                <input class="form-control numberOnly" type="text" name="tel" pattern=".{0}|.{10,}"
                                       maxlength="10" title="กรุณากรอก 10 หลัก" type="text" name="tel"
                                       value="<?= $tel; ?>" required>
                            </div>
                            <div class="form-group">
                                <label>อีเมล์ *</label>
                                <input class="form-control" type="email" name="email" value="<?= $email; ?>" required>
                            </div>


                            <center>
                                <button type="submit" class="btn btn-success">บันทึก</button>
                                <button type="reset" class="btn btn-warning">รีเซต</button>
                            </center>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>




</body>
</html>
