<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">บันทึกระดับน้ำตาลในเลือด</h4>
            </div>
        </div>

        <?PHP
        $member_id = check_session('member_id');

        if (check_session("member_status") == "ADMIN") {
            if (isset($_GET['member_id'])) {
                $member_id = $_GET['member_id'];
            }
        }

        ?>

        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ประวัติการบันทึกน้ำตาลในเลือดทั้งหมด
                </div>
                <div class="panel-body">

                    <?PHP if (check_session("member_status") == "ADMIN") { ?>
                        <form action="" method="get">


                            <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                                <label class="col-md-3 control-label"
                                       style="text-align: right; padding-top: 5px;">สมาชิก *</label>

                                <div class="col-md-5" style="padding-top: 4px;">
                                    <?PHP
                                    $sql = "SELECT * FROM member WHERE member_status != 'DOCTOR'";
                                    $member = result_array($sql);
                                    ?>
                                    <select name="member_id" class="form-control" required>
                                        <option disabled selected value="">เลือกสมาชิก</option>
                                        <?PHP foreach ($member as $mb) { ?>
                                            <option <?=$member_id == $mb['member_id'] ? "selected":"";?>
                                                value="<?= $mb['member_id'] ?>"><?= $mb['member_name'] ?></option>
                                        <?PHP } ?>
                                    </select>
                                </div>

                                <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <hr>
                    <?PHP } ?>


                    <div class="row" style="margin-bottom: 30px;">
                        <form action="save_glycemic_confirm.php" method="post">
<!--                        <form action="process/bloodsugar_process.php" method="post">-->
                            <input type="hidden" name="id" value="">
                            <input type="hidden" name="member_id" value="<?= $member_id; ?>">

                            <div class="form-group">
                                <label class="col-md-2 control-label"
                                       style="font-size: 18px; text-align: right; padding-top: 5px;">ช่วงเวลา</label>

                                <div class="col-md-3" style="padding-top: 4px;">
                                    <?PHP
                                    $sql = "SELECT * FROM duration";
                                    $duration = result_array($sql);
                                    ?>
                                    <select name="duration_id" class="form-control" required>
                                        <option disabled selected value="">เลือกช่วงเวลา</option>
                                        <?PHP foreach ($duration as $dt) { ?>
                                            <option
                                                value="<?= $dt['duration_id'] ?>"><?= $dt['duration_name'] ?></option>
                                        <?PHP } ?>
                                    </select>
                                </div>

                                <label class="col-md-2 control-label"
                                       style="font-size: 18px; text-align: right;  padding-top: 5px;">ค่าระดับน้ำตาล</label>


                                <div class="col-md-2" style="padding-top: 4px;">
                                    <input type="text" name="bloodsugar_number" maxlength="3"
                                           class="form-control numberOnly" required>
                                </div>


                                <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                    <button type="submit" class="btn btn-info">
                                        <i class="fa fa-plus"></i>
                                        เพิ่มรายการ
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>


                    <?PHP
                    $sql = "SELECT * FROM bloodsugar a INNER JOIN duration b ON a.duration_id = b.duration_id INNER JOIN member d ON a.member_id = d.member_id WHERE a.member_id = '{$member_id}' order by date_time desc";
                    $query = result_array($sql);
                    ?>
                    <table class="table table-bordered table-striped" id="table-js">
                        <thead>
                        <tr>
                            <th width="50" class="text-center">ลำดับ</th>
                            <th class="text-center">ชื่อ-นามสกุล</th>
                            <th width="120" class="text-center">ช่วงเวลา</th>
                            <th width="150" class="text-center">ระดับน้ำตาลในเลือด</th>
                            <th width="150" class="text-center">อินซูลินที่แนะนำ</th>
                            <th width="150" class="text-center">อินซูลินที่ฉีดจริง</th>
                            <th width="90" class="text-center">วันที่บันทึก</th>
                            <th width="90" class="text-center">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?PHP foreach ($query as $key => $row) { ?>
                            <tr>
                                <td class="text-center"><?= $key + 1; ?></td>
                                <td class="text-center"><?= $row['member_name']; ?></td>
                                <td class="text-center"><?= $row['duration_name']; ?></td>
                                <td class="text-center"><?= $row['bloodsugar_number']; ?></td>
                                <td class="text-center"><?= $row['bloodsugar_result']; ?></td>
                                <td class="text-center"><?= $row['rISF']; ?></td>
                                <td class="text-center"><?= $row['date_time']; ?></td>
                                <td class="text-center">
                                    <a href="edit_glycemic.php?id=<?= $row['bloodsugar_id']; ?>&member_id=<?=$member_id;?>"
                                       class="btn btn-warning btn-xs">แก้ไข</a>
                                    <a href="process/delete.php?table=bloodsugar&ff=bloodsugar_id&id=<?= $row['bloodsugar_id']; ?>"
                                       onclick="return confirm('ยืนยันการลบ')" class="btn btn-danger btn-xs">ลบ</a>
                                </td>
                            </tr>
                        <?PHP } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form role="form" action="process/food_news.php" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มอาหารใหม่</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>ชื่อ *</label>
                        <input class="form-control" type="text" name="name" maxlength="30"
                               required>
                    </div>
                    <div class="form-group">
                        <label>ปริมานคาร์โบไฮเดรต *</label>
                        <input class="form-control numberOnly" type="text" name="food_gram_carb"
                               maxlength="5" type="text" name="tel"
                               required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP include 'include/footer.php'; ?>


</body>
</html>
