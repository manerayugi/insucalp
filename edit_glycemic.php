<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">บันทึกระดับน้ำตาลในเลือด</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        ฟอร์มบันทึกระดับน้ำตาลในเลือด
                    </div>

                    <?PHP
                    $id = $_GET['id'];

                    $member_id = check_session('member_id');

                    if (check_session("member_status") == "ADMIN") {
                        if (isset($_GET['member_id'])) {
                            $member_id = $_GET['member_id'];
                        }
                    }

                    $sql = "select * from bloodsugar WHERE bloodsugar_id = '{$id}'";
                    $query = row_array($sql);

                    $duration_id = $query['duration_id'];
                    $bloodsugar_number = $query['bloodsugar_number'];
                    $MAX_blsu_now = $query['MAX_blsu_now'];
                    $date_time = $query['date_time'];



                    ?>

                    <div class="panel-body">
                        <form role="form" action="process/bloodsugar_process.php" method="post" enctype="multipart/form-data">

                            <input type="hidden" name="id" value="<?= $id ?>">
                            <input type="hidden" name="MAX_blsu_now" value="<?= $MAX_blsu_now ?>">
                            <input type="hidden" name="member_id" value="<?= $member_id ?>">
                            <input type="hidden" name="date_time_new" value="<?= $date_time ?>">


                            <div class="form-group">
                                <label>เลือกช่วงเวลา *</label>
                                <select name="duration_id" class="form-control" required>
                                    <option disabled selected value="">เลือกช่วงเวลา</option>
                                    <?PHP
                                    $sql = "SELECT * FROM duration";
                                    $duration = result_array($sql);
                                    ?>
                                    <?PHP foreach($duration as $dt){ ?>
                                        <option <?=$dt['duration_id'] == $duration_id ? "selected":"";?>
                                            value="<?=$dt['duration_id']?>"><?=$dt['duration_name']?></option>
                                    <?PHP } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>ค่าระดับน้ำตาล *</label>
                                <input type="text" name="bloodsugar_number" maxlength="3" value="<?=$bloodsugar_number;?>" class="form-control numberOnly" required>
                            </div>


                            <center>
                                <button type="submit" class="btn btn-success">บันทึก</button>
                                <button type="reset" class="btn btn-warning">รีเซต</button>
                            </center>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>




</body>
</html>
