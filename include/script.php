<?PHP include 'config/database.php'; ?>
<?PHP include 'config/fnc.php'; ?>
<?PHP check_login('member_id', 'login.php'); ?>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <title>InsuCal</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="assets/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/css/style.css" rel="stylesheet"/>

    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/bootstrap.js"></script>

    <script src="assets/js/highcharts/highcharts.js"></script>
    <script src="assets/js/highcharts/exporting.js"></script>

    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>

    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>

    <script src="assets/js/datepicker/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/js/datepicker/ui.css">

    <style>
        div.ui-datepicker {
            font-size: 12px;
        }
		p {
			display: block;
			margin-top: 1em;
			margin-bottom: 1em;
			margin-left: 0;
			margin-right: 0;
			text-align: justify;
			text-indent: 30px;
			font-size: 20px;
			}
		tab1 { padding-left: 4em; }
		pp{
			display: block;
			margin-top: 0;
			margin-bottom: 0;
			margin-left: 5px;
			margin-right: 5px;
			text-align: justify;
			font-size: 20px;
			font-weight:bold;
		}		
		pp1{
			display: block;
			margin-top: 0;
			margin-bottom: 0;
			margin-left: 5px;
			margin-right: 5px;
			text-align: justify;
			text-indent: 15px;
			font-size: 20px;
		}
    </style>

    <script>
        $(document).ready(function () {
            $('#table-js').dataTable();

            $("#date_next").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '1',
                maxDate: '+30D',
            });

            $('input.numberOnly').keyup(function (e) {
                if (/\D/g.test(this.value)) {
                    // Filter non-digits from input value.
                    this.value = this.value.replace(/\D/g, '');
                }
            });

            $('input.numberDot').keyup(function (e) {
                var val = $(this).val();
                if(isNaN(val)){
                    val = val.replace(/[^0-9\.]/g,'');
                    if(val.split('.').length>2)
                        val =val.replace(/\.+$/,"");
                }
                $(this).val(val);
            });
        });
    </script>

<?PHP
$member_id = check_session("member_id");
$member_status = check_session("member_status");

$directoryURI = basename($_SERVER['SCRIPT_NAME']);

if (($member_status == "ADMIN" || $member_status == "USER") && $directoryURI != "update_user.php") {

    $sql = "SELECT * FROM information WHERE member_id = '{$member_id}'";
    $query = row_array($sql);

    if (!$query) {
        echo "<meta charset='utf-8'/><script>alert('กรุณากรุณาอัพเดทข้อมูลการรักษาก่อนใช้งานระบบ!!!!{$_SERVER['QUERY_STRING']}');location.href='update_user.php';</script>";
    }
}

?>