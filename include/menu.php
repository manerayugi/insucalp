<div class="navbar navbar-inverse set-radius-zero">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"
               style="color: #428bca; font-size: 18px; font-weight: bold; margin-top: 15px; position: relative; padding-left: 85px;">
                <img src="assets/img/logo.png"
                     style="position: absolute; top: 0; left: 0; width: auto; height: 60px;" alt="">
            </a>

        </div>

        <div class="right-div">
            <a href="process/logout.php" class="btn btn-danger pull-right"><i class="fa fa-sign-out"></i> ออกจากระบบ</a>

            <a href="user_profile.php" style="margin-right: 10px;" class="btn btn-success pull-right"><i
                    class="fa fa-check"></i> ข้อมูลส่วนตัว</a>


            <p style="margin: 5px 10px;" class="pull-right">( <?= member_status(check_session("member_status")); ?> )
                คุณ : <?= check_session("member_name"); ?></p>
        </div>
    </div>
</div>

<section class="menu-section">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <div class="navbar-collapse collapse ">
                    <ul id="menu-top" class="nav navbar-nav navbar-right">
                        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>

                        <?PHP if (check_session("member_status") == "ADMIN") { ?>
                            <li><a href="save_glycemic.php"><i class="fa fa-check"></i> บันทึกระดับน้ำตาลในเลือด </a>
                            </li>
                            <li><a href="save_food.php"><i class="fa fa-list"></i> บันทึกการทานอาหาร</a></li>
                            <li><a href="report.php"><i class="fa fa-table"></i> ตารางรายงานผล</a></li>
                            <li><a href="exercise.php"><i class="fa fa-rub"></i> การออกกำลังกาย</a></li>


                            <li>
                                <a href="#" class="dropdown-toggle" id="a2" data-toggle="dropdown"><i
                                        class="fa fa-pencil"></i> จัดการข้อมูล
                                    <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu sub-menu" role="menu" aria-labelledby="a2">
                                    <li><a href="doctor.php"><i class="fa fa-hand-o-right"></i> ข้อมูลแพทย์</a></li>
                                    <li><a href="user.php"><i class="fa fa-hand-o-right"></i> ข้อมูลสมาชิก</a></li>
                                    <li><a href="food.php"><i class="fa fa-hand-o-right"></i> ข้อมูลอาหาร</a></li>
                                </ul>
                            </li>

                        <?PHP } elseif (check_session("member_status") == "DOCTOR") { ?>

                            <li><a href="report.php"><i class="fa fa-table"></i> ตารางรายงานผล</a></li>
                            <li><a href="exercise.php"><i class="fa fa-rub"></i> การออกกำลังกาย</a></li>

                        <?PHP } elseif (check_session("member_status") == "USER") { ?>

                            <li><a href="save_glycemic.php"><i class="fa fa-check"></i> บันทึกระดับน้ำตาลในเลือด </a>
                            </li>
                            <li><a href="save_food.php"><i class="fa fa-list"></i> บันทึกการทานอาหาร</a></li>
                            <li><a href="report.php"><i class="fa fa-table"></i> ตารางรายงานผล</a></li>
                            <li><a href="exercise.php"><i class="fa fa-rub"></i> การออกกำลังกาย</a></li>

                        <?PHP } ?>

                        <li><a href="by.php"><i class="fa fa-user"></i> ผู้จัดทำ</a></li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>