<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
    <?PHP date_default_timezone_set('Asia/Bangkok'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        /* Popup container - can be anything you want */
        .popup {
            position: relative;
            display: inline-block;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* The actual popup */
        .popup .popuptext {
            visibility: hidden;
            width: 160px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 8px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -80px;
        }

        /* Popup arrow */
        .popup .popuptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        /* Toggle this class - hide and show the popup */
        .popup .show {
            visibility: visible;
            -webkit-animation: fadeIn 1s;
            animation: fadeIn 1s;
        }

        /* Add animation (fade in the popup) */
        @-webkit-keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    </style>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<!--<div class="content-wrapper">-->
<div class="container">
    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">อัพเดทข้อมูลการรักษา</h4>
        </div>
    </div>

    <?PHP
    $id = check_session("member_id");
    $url = "index.php";

    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $url = "user.php";
    }

    $sql = "SELECT * FROM member WHERE member_id = '{$id}'";
    $member = row_array($sql);

    ?>

    <div class="row">
        <div class="col-md-7 col-md-offset-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ข้อมูลการรักษา : <?= $member['member_name']; ?>
                </div>

                <?PHP

                $check = 1;

                $weight = '';
                $height = '';
                $ICR_br = '';
                $ICR_lu = '';
                $ICR_di = '';
                $ISF_br = '';
                $ISF_lu = '';
                $ISF_di = '';
                $MAX_blsu = '';
                $doctor = '';
                $age = '';
                $byear = '';
                $itype = '';
                $tbrs = '';
                $tbre = '';
                $tlus = '';
                $tlue = '';
                $tdis = '';
                $tdie = '';


                $sql = "SELECT * FROM information WHERE member_id = '{$id}'";
                $query = row_array($sql);

                if ($query) {

                    $check = 0;

                    $age = $query['age'];
                    $weight = $query['weight'];
                    $height = $query['height'];
                    $ICR_br = $query['ICR_br'];
                    $ICR_lu = $query['ICR_lu'];
                    $ICR_di = $query['ICR_di'];
                    $ISF_br = $query['ISF_br'];
                    $ISF_lu = $query['ISF_lu'];
                    $ISF_di = $query['ISF_di'];
                    $MAX_blsu = $query['MAX_blsu'];
                    $doctor = $query['doctor'];
                    $bday = $query['bday'];
                    $bmount = $query['bmount'];
                    $byear = $query['byear'];
                    $itype = $query['itype'];
                    $tbrs = $query['tbrs'];
                    $tbre = $query['tbre'];
                    $tlus = $query['tlus'];
                    $tlue = $query['tlue'];
                    $tdis = $query['tdis'];
                    $tdie = $query['tdie'];
                    if ($tbrs == NULL OR $tbre == NULL
                        OR $tlus == NULL OR $tlue == NULL
                        OR $tdis == NULL OR $tdie == NULL) {
                        $tbrs = 4;
                        $tbre = 10;
                        $tlus = 10;
                        $tlue = 16;
                        $tdis = 16;
                        $tdie = 21;
                    }
                }


                ?>

                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="col-md-12">
                                <form role="form" action="process/user_update.php" class="" method="post"
                                      enctype="multipart/form-data">

                                    <input type="hidden" name="id" value="<?= $id ?>">
                                    <input type="hidden" name="check" value="<?= $check ?>">
                                    <input type="hidden" name="url" value="<?= $url ?>">

                                    <?PHP if ($check) { ?>
                                        <p class="text-center" style="color: red;"> *** <?= $member['member_name']; ?> :
                                            ยังไม่เคยกรอกข้อมูล ***</p>
                                    <?PHP } ?>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">แพทย์ประจำตัว</label>
                                        <div class="col-md-3">
                                            <select name="doctor" class="form-control" required>
                                                <option value="0"<?PHP if ($doctor == 0) {
                                                    echo "selected";
                                                } ?> >ไม่มีแพทย์ประจำตัว
                                                </option>
                                                <?PHP
                                                $sql = "SELECT * FROM member WHERE member_status = 'DOCTOR'";
                                                $cc = result_array($sql);
                                                ?>

                                                <?PHP foreach ($cc as $_cc) { ?>
                                                    <option value="<?= $_cc['member_id']; ?>" <?PHP if ($_cc['member_id'] == $doctor) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $_cc['member_name']; ?></option>
                                                <?PHP } ?>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">วัน/เดือน/ปี เกิด</label>
                                        <div class="col-md-3">
                                            <select name="bday" class="form-control" required>
                                                <option value="" disabled selected>วัน</option>
                                                <?PHP $bd = 0;
                                                for ($bd = 1; $bd < 32; $bd++) { ?>
                                                    <option value="<?= ($bd <= 9 ? '0' . $bd : $bd) ?>" <?PHP if ($bd == $bday) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $bd; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="bmount" class="form-control" required>
                                                <option value="" disabled selected>เดือน</option>
                                                <?PHP $bm = 0;
                                                for ($bm = 1; $bm < 13; $bm++) { ?>
                                                    <option value="<?= ($bm <= 9 ? '0' . $bm : $bm) ?>" <?PHP if ($bm == $bmount) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= mount_name($bm); ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="byear" class="form-control" required>
                                                <option value="" disabled selected>ปี</option>
                                                <?PHP $by = date("Y");
                                                for ($by; $by > date("Y") - 101; $by--) { ?>
                                                    <option value="<?= $by; ?>" <?PHP if ($by == $byear) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $by; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">น้ำหนัก</label>
                                        <div class="col-md-2">
                                            <input class="form-control numberOnly" type="text" name="weight"
                                                   maxlength="5"
                                                   value="<?= $weight; ?>"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">ส่วนสูง</label>
                                        <div class="col-md-2">
                                            <input class="form-control numberOnly" type="text" name="height"
                                                   maxlength="5"
                                                   value="<?= $height; ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>ประเภทของอินซูลินที่ใช้ <i style="color: red;">( หากไม่ทราบให้เลือก
                                                อินซูลินออกฤทธิ์เร็ว ) </i></label>
                                        <div class="popup" onclick="myFunction()"><i class='fa fa-info-circle'></i>
                                            <span class="popuptext" id="myPopup"><strong>อินซูลินออกฤทธิ์เร็ว</strong> เช่น<br>
                                            Humalog, Novorapid, Novolog, Apidra<br>
                                        <strong>อินซูลินออกฤทธิ์สั้น</strong> เช่น<br>
                                            Humulin R, Actrapid HM
                                    </span>
                                        </div>
                                        <select name="itype" class="form-control" required>
                                            <option value="" disabled selected>ประเภทของอินซูลิน</option>
                                            <option value="1" <?PHP if ($itype == 1) {
                                                echo "selected";
                                            } ?> >อินซูลินออกฤทธิ์เร็ว(Rapid-acting insulin analog)
                                            </option>
                                            <option value="2" <?PHP if ($itype == 2) {
                                                echo "selected";
                                            } ?> >อินซูลินออกฤทธิ์สั้น(Short-acting insulin)
                                            </option>
                                        </select>


                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">ช่วงเวลาอาหารเช้า</label>
                                        <div class="col-md-3">
                                            <select name="tbrs" class="form-control" required>
                                                <option value="" disabled selected>นาฬิกา</option>
                                                <?PHP $brs = 0;
                                                for ($brs = 0; $brs < 25; $brs++) { ?>
                                                    <option value="<?= ($brs <= 9 ? '0' . $brs : $brs) ?>" <?PHP if ($brs == $tbrs) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $brs; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            ถึง
                                        </div>
                                        <div class="col-md-3">
                                            <select name="tbre" class="form-control" required>
                                                <option value="" disabled selected>นาฬิกา</option>
                                                <?PHP $bre = 0;
                                                for ($bre = $tbrs; $bre < 25; $bre++) { ?>
                                                    <option value="<?= ($bre <= 9 ? '0' . $bre : $bre) ?>" <?PHP if ($bre == $tbre) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $bre; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">ช่วงเวลาอาหารกลางวัน</label>
                                        <div class="col-md-3">
                                            <select name="tlus" class="form-control" required>
                                                <option value="" disabled selected>นาฬิกา</option>
                                                <?PHP $lus = 0;
                                                for ($lus = $tbre; $lus < 25; $lus++) { ?>
                                                    <option value="<?= ($lus <= 9 ? '0' . $lus : $lus) ?>" <?PHP if ($lus == $tlus) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $lus; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            ถึง
                                        </div>
                                        <div class="col-md-3">
                                            <select name="tlue" class="form-control" required>
                                                <option value="" disabled selected>นาฬิกา</option>
                                                <?PHP $lue = 0;
                                                for ($lue = $tlus; $lue < 25; $lue++) { ?>
                                                    <option value="<?= ($lue <= 9 ? '0' . $lue : $lue) ?>" <?PHP if ($lue == $tlue) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $lue; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">ช่วงเวลาอาหารเย็น</label>
                                        <div class="col-md-3">
                                            <select name="tdis" class="form-control" required>
                                                <option value="" disabled selected>นาฬิกา</option>
                                                <?PHP $dis = 0;
                                                for ($dis = $tlue; $dis < 25; $dis++) { ?>
                                                    <option value="<?= ($dis <= 9 ? '0' . $dis : $dis) ?>" <?PHP if ($dis == $tdis) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $dis; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            ถึง
                                        </div>
                                        <div class="col-md-3">
                                            <select name="tdie" class="form-control" required>
                                                <option value="" disabled selected>นาฬิกา</option>
                                                <?PHP $die = 0;
                                                for ($die = $tdis; $die < 25; $die++) { ?>
                                                    <option value="<?= ($die <= 9 ? '0' . $die : $die) ?>" <?PHP if ($die == $tdie) {
                                                        echo "selected";
                                                    } ?> >
                                                        <?= $die; ?>
                                                    </option>
                                                <?PHP } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-5">ระดับน้ำตาลในเลือดเป้าหมาย</label>
                                        <input class="form-control numberOnly" type="text" name="MAX_blsu" maxlength="5"
                                               value="<?= $MAX_blsu; ?>"
                                               required>
                                    </div>

                                    <input type="hidden" name="member_insulinRec" value="<?= $member["member_insulinRec"] ?>">
                                    <?PHP
                                    if ($member["member_insulinRec"] == 2) {
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="text-align: center">อินซูลินต่อกรับของคาร์โบไฮเดรต
                                                (ICR) : </label>
                                            <div class="col-md-3">
                                                <label class="control-label">เช้า 1 :</label>
                                                <input class="form-control numberOnly" type="text" name="ICR_br"
                                                       maxlength="5" style="width: 50px"
                                                       value="<?= $ICR_br; ?>" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">กลางวัน 1 :</label>
                                                <input class="form-control numberOnly" type="text" name="ICR_lu"
                                                       maxlength="5" style="width: 50px"
                                                       value="<?= $ICR_lu; ?>" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">เย็น 1 :</label>
                                                <input class="form-control numberOnly" type="text" name="ICR_di"
                                                       maxlength="5" style="width: 50px"
                                                       value="<?= $ICR_di; ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="text-align: center">อินซูลินเพื่อแก้ไขน้ำตาลในเลือดสูง
                                                (ISF) : </label>
                                            <div class="col-md-3">
                                                <label class="control-label">เช้า 1 :</label>
                                                <input class="form-control numberOnly" type="text" name="ISF_br"
                                                       maxlength="5" style="width: 50px"
                                                       value="<?= $ISF_br; ?>" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">กลางวัน 1 :</label>
                                                <input class="form-control numberOnly" type="text" name="ISF_lu"
                                                       maxlength="5" style="width: 50px"
                                                       value="<?= $ISF_lu; ?>" required>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">เย็น 1 :</label>
                                                <input class="form-control numberOnly" type="text" name="ISF_di"
                                                       maxlength="5" style="width: 50px"
                                                       value="<?= $ISF_di; ?>" required>
                                            </div>
                                        </div>
                                    <?PHP } ?>


                                    <center>
                                        <button type="submit" class="btn btn-success">บันทึก</button>
                                        <button type="reset" class="btn btn-warning">รีเซต</button>
                                    </center>


                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--</div>-->
<script>
    // When the user clicks on div, open the popup
    function myFunction() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }
</script>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
