<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
    <link rel="stylesheet" href="assets/js/time/jquery.ui.timepicker.css"/>
    <link rel="stylesheet" href="assets/js/time/ui-lightness/jquery-ui-1.10.0.custom.min.css"/>
    <script src="assets/js/time/jquery.ui.timepicker.js"></script>
    <style>
        div.ui-datepicker {
            font-size: 14px;
        }

        .ui-datepicker-month, .ui-datepicker-year {
            color: red;
        }
    </style>
    <script>
        $(document).ready(function () {

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });


            $('.timepicker').timepicker({
                onHourShow: OnHourShowCallback
            });
            function OnHourShowCallback(hour) {
                return true; // valid
            }
        });
    </script>
</head>
<body>
<?PHP include 'include/menu.php';
extract($_POST);
?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">บันทึกการทานอาหาร</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        ฟอร์มบันทึกการทานอาหาร
                    </div>

                    <?PHP
                    $duration_id = "";
                    $member_id = check_session("member_id");

                    if (isset($_GET['duration_id'])) {
                        $duration_id = $_GET['duration_id'];
                    }

                    $hour = date("H");

                    $sql1 = "SELECT * FROM information WHERE member_id = '{$member_id}'";
                    $information = row_array($sql1);
                    $age = $information['age'];
                    $tbrs = $information['tbrs'];
                    $tbre = $information['tbre'];
                    $tlus = $information['tlus'];
                    $tlue = $information['tlue'];
                    $tdis = $information['tdis'];
                    $tdie = $information['tdie'];
                    $startHR = 0;
                    $endHR = 0;
                    if ($duration_id == 1 OR $duration_id == 2) {
                        $startHR = $tbrs;
                        $endHR = $tbre;
                    } elseif ($duration_id == 3 OR $duration_id == 4) {
                        $startHR = $tlus;
                        $endHR = $tlue;
                    } elseif ($duration_id == 5 OR $duration_id == 6) {
                        $startHR = $tdis;
                        $endHR = $tdie;
                    } else {
                        $startHR = 0;
                        $endHR = 0;
                    }
                    ?>


                    <div class="panel-body">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-4 control-label"
                                       style="text-align: right; padding-top: 5px;">เลือกช่วงเวลา *</label>

                                <div class="col-md-5" style="padding-top: 4px;">
                                    <select name="duration_id" class="form-control" style="background: #eee;" required>
                                        <?PHP
                                        $sql = "SELECT * FROM duration WHERE duration_status = 1 AND duration_id = '{$duration_id}'";
                                        $duration = result_array($sql);
                                        ?>
                                        <?PHP foreach ($duration as $dt) { ?>
                                            <option <?= $duration_id == $dt['duration_id'] ? "selected" : ""; ?>
                                                value="<?= $dt['duration_id'] ?>">
                                                <?= str_replace("ก่อน", "", $dt['duration_name']); ?>
                                            </option>
                                        <?PHP } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <?PHP if (isset($_GET['duration_id'])) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <table
                                        class="table table-condensed table-striped table-bordered table-hover no-margin">
                                        <thead>
                                        <tr>
                                            <th style="width:50px">ลำดับ</th>
                                            <th style="" class="text-center">รายการอาหาร</th>
                                            <th style="width: 100px;" class="text-center">จำนวนที่ทาน</th>
                                            <th style="width: 120px;" class="text-center">คาร์โบไฮเดรต (g)</th>
                                            <th style="width: 100px;" class="text-center">รวม (g)</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?PHP $sum = 0; ?>

                                        <?PHP if ($item = $_SESSION['cart_food']) { ?>

                                            <?PHP foreach ($item as $key => $_item) { ?>

                                                <?PHP
                                                $list_total = $_item['carb'] * $_item['qty'];
                                                $sum = $sum + $list_total;
                                                ?>

                                                <tr>
                                                    <td><?PHP echo $key + 1; ?></td>
                                                    <td><?PHP echo $_item['name']; ?></td>
                                                    <td class="text-center">
                                                        <?= $_item['qty'] ?> g
                                                    </td>
                                                    <td class="text-center"><?PHP echo number_format($_item['carb'], 2); ?>
                                                        g
                                                    </td>
                                                    <td class="text-center"><?PHP echo number_format($list_total, 2); ?>
                                                        g
                                                    </td>
                                                </tr>
                                            <?PHP } ?>
                                        <?PHP } else { ?>
                                            <tr>
                                                <td colspan="7"
                                                    style="color: red; text-align: center; padding: 10px;">
                                                    ไม่มีอาหาร
                                                </td>
                                            </tr>
                                        <?PHP } ?>

                                        <?PHP $_SESSION['cart_food_sum'] = $sum; ?>

                                        <tr>
                                            <td colspan="4" style="text-align: right;">คาร์โบไฮเดรตรวมทั้งหมด</td>
                                            <td colspan="2"> <?PHP echo number_format(check_session('cart_food_sum'), 2); ?>
                                                g
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <?PHP

                            $sql = "SELECT * FROM information WHERE member_id = '{$member_id}'";
                            $information = row_array($sql);

                            if ($duration_id == 1) {
                                $now_time = "อินซูลินสำหรับทานอาหารเช้า";
                                $insulin_check = $information['ICR_br'];
                            } elseif ($duration_id == 3) {
                                $now_time = "อินซูลินสำหรับทานอาหารกลางวัน";
                                $insulin_check = $information['ICR_lu'];
                            } elseif ($duration_id == 6) {
                                $now_time = "อินซูลินสำหรับทานอาหารเย็น";
                                $insulin_check = $information['ICR_di'];
                            } else {
                                if ($hour >= 6 && $hour < 12) {
                                    $now_time = "อินซูลินสำหรับทานอาหารเช้า";
                                    $insulin_check = $information['ICR_br'];
                                } elseif ($hour >= 12 && $hour < 18) {
                                    $now_time = "อินซูลินสำหรับทานอาหารกลางวัน";
                                    $insulin_check = $information['ICR_lu'];
                                } else {
                                    $now_time = "อินซูลินสำหรับทานอาหารเย็น";
                                    $insulin_check = $information['ICR_di'];
                                }


                            }

                            $insulin = floor($sum / $insulin_check);

                            $s_date = date("Y-m-d");
                            $s_time = date("H:i:s");

                            if (isset($_GET['s_date'])) {
                                $s_date = $_GET['s_date'];
                            }
                            if (isset($_GET['s_time'])) {
                                $s_time = $_GET['s_time'];
                            }

                            ?>

                            <hr>
                            <h3 class="text-center" style="font-size: 16px; line-height: 26px;">
                                <b><?= $now_time; ?></b>
                                <br>
                                อินซูลิน 1 unit ต่อ <?= $insulin_check; ?> กรัมของคาร์โบไฮเดรต
                                <br>
                                คาร์โบไฮเดรตรวมทั้งหมด <?= $sum; ?> กรัม
                                <br><br>
                                <b style="font-size: 20px; color: red;">
                                    อินซูลินที่แนะนำ <?=$insulin;?> Unit
                                    <?PHP $rinsulin = $insulin; ?>
                                </b><br>
                                <form action="process/eat_process.php" method="post">
                                อินซูลินที่ฉีดจริง : <input type="text" name="rinsulin" maxlength="3"
                                       value="<?= $rinsulin; ?>" > unit
                            <br>
                            </h3>

                            <hr>
                            <div class="form-group" style=" text-align: center; margin-bottom: 20px; overflow: hidden;">
                                <label class="col-md-3 control-label"
                                       style="text-align: center; padding-top: 5px;">ช่วงเวลา *</label>

                                <div class="col-md-3" style="padding-top: 4px;">
                                    <input style="width: 100px" type="text" name="s_date" value="<?= $s_date; ?>"
                                           class="form-control datepicker" placeholder="วัน" required>
                                </div>
                                <?PHP
                                if ($duration_id > 6) {
                                    ?>
                                    <div class="col-md-3" style="padding-top: 4px;">
                                        <input style="width: 100px" type="text" name="s_time" value="<?= $s_time; ?>"
                                               class="form-control timepicker" placeholder="เวลา" required>
                                    </div>
                                    <?PHP
                                } else {
                                    ?>
                                    <div class="col-md-3">
                                        <select name="thr" class="form-control" required>
                                            <option value="" >นาฬิกา</option>
                                            <?PHP $thr = 0;
                                            for ($thr = $startHR; $thr <= $endHR; $thr++) { ?>
                                                <option value="<?= ($thr <= 9 ? '0' . $thr : $thr) ?>"><?= ($thr <= 9 ? '0' . $thr : $thr) ?></option>
                                            <?PHP } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="tmin" class="form-control" required>
                                            <option value="" >นาที</option>
                                            <?PHP $tmin = 0;
                                            for ($tmin = 0; $tmin <= 60; $tmin++) { ?>
                                                <option value="<?= ($tmin <= 9 ? '0' . $tmin : $tmin) ?>"><?= ($tmin <= 9 ? '0' . $tmin : $tmin) ?></option>
                                            <?PHP } ?>
                                        </select>
                                    </div>

                                <?PHP } ?>
<!--                                <div class="col-md-3" style="padding-top: 4px;">-->
<!--                                    <input type="text" name="s_time" value="--><?//= $s_time; ?><!--"-->
<!--                                           class="form-control timepicker" placeholder="เวลา" required>-->
<!--                                </div>-->
                            </div>


                            <hr>


                                <input type="hidden" name="duration_id" value="<?= $duration_id; ?>">
                                <input type="hidden" name="member_id" value="<?= $member_id; ?>">
                                <input type="hidden" name="insulin_number" value="<?=$insulin;?>">



                                <center>
<!--                                    <b>ฉีดอินซูลิน : </b>-->
<!--                                    <input type="radio" name="insulin_status" value="1" --><?PHP //if ($rinsulin != 0){echo "checked";}?><!-- > ฉีด-->
<!--                                    &nbsp;-->
<!--                                    &nbsp;-->
<!--                                    <input type="radio" name="insulin_status" value="0" --><?PHP //if ($rinsulin == 0){echo "checked";}?><!-- > ไม่ฉีด-->
                                    <hr>
                                    <a href="save_food_form.php?duration_id=<?= $duration_id; ?>"
                                       class="btn btn-warning">ย้อนกลับ</a>
                                    <button type="submit" class="btn btn-success">บันทึกรายการ</button>
                                </center>

                            </form>


                        <?PHP } else { ?>
                            <p class="text-center" style="padding: 20px; color: red; font-size: 22px;">
                                กรุณาเลือกช่วงเวลาก่อนทำรายการ
                            </p>
                        <?PHP } ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
