<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">อาหาร</h4>
            </div>
        </div>

        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">
                    รายการผู้ป่วยทั้งหมด
                    <a href="food_form.php" class="btn btn-primary btn-sm pull-right">เพิ่มข้อมูล</a>
                </div>
                <div class="panel-body">
                    <?PHP
                    $sql = "SELECT * FROM food";
                    $query = result_array($sql);
                    ?>
                    <table class="table table-bordered table-striped" id="table-js">
                        <thead>
                        <tr>
                            <th width="50" class="text-center">ลำดับ</th>
                            <th  class="text-center">ชื่อ-นามสกุล</th>

                            <th width="150" class="text-center">จำนวนคาร์โบไฮเดรต</th>
                            <th width="100" class="text-center">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?PHP foreach ($query as $key=>$row) { ?>
                            <tr>
                                <td class="text-center"><?=$key+1;?></td>
                                <td class="text-center"><?=$row['food_name'];?></td>
                                <td class="text-center"><?=$row['food_gram_carb'];?></td>
                                <td class="text-center">
                                    <a href="food_form.php?id=<?=$row['food_id'];?>" class="btn btn-warning btn-xs">แก้ไข</a>
                                    <a href="process/delete.php?table=food&ff=food_id&id=<?=$row['food_id'];?>" onclick="return confirm('ยืนยันการลบ')" class="btn btn-danger btn-xs">ลบ</a>
                                </td>
                            </tr>
                        <?PHP } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>


</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: MacbookPro
 * Date: 6/7/18 AD
 * Time: 12:46
 */