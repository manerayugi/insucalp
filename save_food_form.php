<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?PHP include 'include/script.php'; ?>



    <link href="assets/css/select2.css" rel="stylesheet"/>
    <script src="assets/js/select2.js"></script>
</head>
<body>
<?PHP include 'include/menu.php'; ?>
<div class="content-wrapper">
    <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">บันทึกการทานอาหาร</h4>
            </div>
        </div>

        <?PHP

        if (!isset($_SESSION['cart_food'])) {
            $_SESSION['cart_food'] = array();
        }

        if (!isset($_SESSION['cart_food_sum'])) {
            $_SESSION['cart_food_sum'] = 0;
        }

        if (isset($_GET['clear'])) {
            $_SESSION['cart_food'] = array();
            $_SESSION['cart_food_sum'] = 0;
        }

        ?>

        <?PHP $member_id = check_session("member_id"); ?>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        ฟอร์มบันทึกการทานอาหาร
                    </div>

                    <?PHP
                    $duration_id = "";

                    if (isset($_GET['duration_id'])) {
                        $duration_id = $_GET['duration_id'];
                    }
                    ?>


                    <div class="panel-body">

                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-4 control-label"
                                       style="text-align: right; padding-top: 5px;">เลือกช่วงเวลา *</label>

                                <div class="col-md-5" style="padding-top: 4px;">
                                    <select name="duration_id" class="form-control" required>
                                        <option disabled selected value="">เลือกช่วงเวลา</option>
                                        <?PHP
                                        $sql = "SELECT * FROM duration WHERE duration_status = 1";
                                        $duration = result_array($sql);
                                        ?>
                                        <?PHP foreach ($duration as $dt) { ?>
                                            <option <?= $duration_id == $dt['duration_id'] ? "selected" : ""; ?>
                                                    value="<?= $dt['duration_id'] ?>">
                                                <?= str_replace("ก่อน", "", $dt['duration_name']); ?>
                                            </option>
                                        <?PHP } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <?PHP if (isset($_GET['duration_id'])) { ?>
                            <div class="row">
                                <form action="process/add_cart_food.php" method="get">

                                    <input type="hidden" name="duration_id" value="<?= $duration_id; ?>">
                                    <input type="hidden" name="qty" value="1">

                                    <div class="form-group" style="margin-bottom: 20px; overflow: hidden;">
                                        <label class="col-md-2 control-label"
                                               style="text-align: right; padding-top: 5px;">อาหาร *</label>

                                        <div class="col-md-5" style="padding-top: 4px;">
                                            <?PHP
                                            if ($member_id == 1){
                                                $sql = "SELECT * FROM food order by food_name ASC ";
                                                $food = result_array($sql);
                                            }else{
                                                $sql = "SELECT * FROM food where member_creat = 1 OR member_creat = '$member_id'order by food_name ASC ";
                                                $food = result_array($sql);
                                            }

                                            ?>
                                            <select id="selectfood" name="food_id" class="form-control" style="width:300px" required>
                                                <option disabled selected value="">โปรดเลือกอาหารที่รับประทาน</option>
                                                <?PHP foreach ($food as $dt) { ?>
                                                    <option
                                                            value="<?= $dt['food_id'] ?>"><?= $dt['food_name'] ?></option>
                                                <?PHP } ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 " style="padding-top: 4px; padding-left: 20px">
                                            <button type="submit" class="btn btn-info">
                                                <i class="fa fa-plus"></i>
                                                เพิ่มรายการ
                                            </button>
                                        </div>
                                        <div class="col-md-1 " style="padding-top: 4px; padding-left: 20px">
                                            <button type="button" class="btn btn-warning" data-toggle="modal"
                                                    data-target="#myModal">
                                                <i class="fa fa-plus"></i>
                                                เพิ่มใหม่
                                            </button>
                                        </div>
                                    </div>
                                </form>

                                <div class="col-md-12">
                                    <table
                                            class="table table-condensed table-striped table-bordered table-hover no-margin">
                                        <thead>
                                        <tr>
                                            <th style="width:50px">ลำดับ</th>
                                            <th style="" class="text-center">รายการอาหาร</th>
                                            <th style="width: 100px;" class="text-center">จำนวนที่ทาน</th>
                                            <th style="width: 120px;" class="text-center">คาร์โบไฮเดรต (g)</th>
                                            <th style="width: 100px;" class="text-center">รวม (g)</th>
                                            <th style="width:50px;" class="hidden-phone">จัดการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?PHP $sum = 0; ?>

                                        <?PHP if ($item = $_SESSION['cart_food']) { ?>

                                            <?PHP foreach ($item as $key => $_item) { ?>

                                                <?PHP
                                                $list_total = $_item['carb'] * $_item['qty'];
                                                $sum = $sum + $list_total;
                                                ?>

                                                <tr>
                                                    <td><?PHP echo $key + 1; ?></td>
                                                    <td><?PHP echo $_item['name']; ?></td>
                                                    <td class="text-center">
                                                        <input type="text" name="qty" class="numberDot"
                                                               id="<?= $_item['pid']; ?>" style="width: 50px;"
                                                               maxlength="5" value="<?= $_item['qty'] ?>">
                                                    </td>
                                                    <td class="text-center"><?PHP echo number_format($_item['carb'], 2); ?>
                                                        g
                                                    </td>
                                                    <td class="text-center"><?PHP echo number_format($list_total, 2); ?>
                                                        g
                                                    </td>
                                                    <td>
                                                        <a href="process/del_cart_food.php?id=<?PHP echo $_item['pid']; ?>&duration_id=<?= $duration_id ?>"
                                                           onclick="return confirm('ต้องการลบรายการนี้?');">
                                                            <button type="button" class="btn btn-xs btn-danger"
                                                                    data-original-title="">ลบ
                                                            </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?PHP } ?>
                                        <?PHP } else { ?>
                                            <tr>
                                                <td colspan="7"
                                                    style="color: red; text-align: center; padding: 10px;">
                                                    ไม่มีอาหาร
                                                </td>
                                            </tr>
                                        <?PHP } ?>

                                        <?PHP $_SESSION['cart_food_sum'] = $sum; ?>

                                        <tr>
                                            <td colspan="4" style="text-align: right;">คาร์โบไฮเดรตรวมทั้งหมด</td>
                                            <td colspan="2"> <?PHP echo number_format(check_session('cart_food_sum'), 2); ?>
                                                g
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <hr>


                            <center>
                                <a href="save_food_confirm.php?duration_id=<?= $duration_id; ?>"
                                   class="btn btn-success">ขั้นตอนต่อไป</a>
                            </center>

                        <?PHP } else { ?>
                            <p class="text-center" style="padding: 20px; color: red; font-size: 22px;">
                                กรุณาเลือกช่วงเวลาก่อนทำรายการ
                            </p>
                        <?PHP } ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?PHP include 'include/footer.php'; ?>

<script>
    $(document).ready(function () {
        $("select[name='duration_id']").change(function () {
            var duration_id = $(this).val();

            location.href = "save_food_form.php?duration_id=" + duration_id;
        });
    });

    $('input[name="qty"]').change(function () {
        var food_id = $(this).attr('id');
        var qty = $(this).val();
        var duration_id = '<?=$duration_id;?>';

        location.href = "process/add_cart_food.php?duration_id=" + duration_id + "&food_id=" + food_id + "&qty=" + qty;
    });

    $(document).ready(function() {
        $("#selectfood").select2({
            placeholder: "โปรดเลือกอาหารที่รับประทาน",
            allowClear: true
        });
    });
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form role="form" action="process/food_news.php" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มอาหารใหม่</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>ชื่อ *</label>
                        <input class="form-control" type="text" name="name" maxlength="30"
                               required>
                    </div>
                    <div class="form-group">
                        <label>ปริมานคาร์โบไฮเดรต *</label>
                        <input class="form-control numberOnly" type="text" name="food_gram_carb"
                               maxlength="5" type="text" name="tel"
                               required>
                    </div>
                    <input type="hidden" name="member_id" value="<?= $member_id; ?>">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </div>
        </form>
    </div>
</div>


</body>
</html>
